%% test.tex
%% Pawel Heldt
%% 02 Feb 21

\documentclass{mpaper}

\begin{document}

\title{On the use of Unsupervised Learning for Online Outliers Detection in Multivariate Data Streams}
\author{Pawel Heldt}
\matricnum{2268686}

\maketitle

\begin{abstract}
With the advancement of data analytic more and more data is being gathered. The number of devices collecting it and the decentralised nature of infrastructure they are part of making them prone to measurements error that could be later misinterpreted in the analysis. Research into outlier detection algorithms, especially those which can reliably scan one-dimensional time-series data for anomalies has a huge potential to further improve current data handling. Within this paper, two different machine learning methods: a One Class Supported Vector Machine and a Principal Component Analysis were adapted to look for outliers within time-series data. Replacing models based on hard-coded heuristics with ones utilising machine learning could offer a more flexible outlier detection that could efficiently even with time-series exposed to under dynamic significantly altering circumstances.
\end{abstract}

\section{Experiment}

\subsection{Evaluation of One-Class SVM}
Looking for best parameters' configuration for One-Class SVM classification method tunable variables included three separate strategies of processing time-series, different sliding window sizes, tweaking of model's internal \textnu{} parameter, which defines the percentage of data-points classified as the outliers throughout the training, and various time-series folding criteria. As the first step of the analysis, the results had been aggregated based on each tweakable parameter and had their ratios-to-baseline compared. Overall average naive-detection-to-baseline-ratio ended up being 0.75 (with 0.72 median) for AAPL tweets time-series and 0.64 (with 0.53 median) for NYC Taxis' time-series. For indepth-detection-to-baseline-ratio those values were respectively 1.31 (with median 1.07) and 1.33 (with median 0.88). The initial findings suggested that only by considering all occurrences of outliers throughout multiple data-points it is possible to beat the baseline's performance. After comparing individual parameters' values aggregations indepth-to-baseline-ratio is always higher than its naive equivalents. That relation can be seen in the graph attached below which highlights the fact that a detection which distinguishes elements of the folded data-points individually outperforms one which only treats a datapoint vector as a unitary element.
\includegraphics[scale=0.20]{graphics/indepth_vs_naive_detection_comparison.png}\\

\subsubsection{Simulation-type}
\includegraphics[scale=0.20]{graphics/indepth_param_vis/simulation_type_indepth_vis.png}

\begin{tabular}{ |p{1.6cm}||p{1.6cm}|p{1.6cm}|p{1.6cm}|  }
 \hline
 \multicolumn{4}{|c|}{Performance Based on Simulation Type }\\
 \multicolumn{4}{|c|}{for AAPL Tweets (and NYC Taxi)} \\
 \hline
 Sim. Type & Avg. Indepth Det. Ratio To Baseline & Avg. Indepth Det. Ratio & Avg. Over-fitting Ratio  \\
 \hline
 Continuous & 1.44 (1.27) & 0.45 (0.52) & 0.18 (0.21)\\
 Dynamic & 1.55 (1.47) & 0.39 (0.5) & 0.14 (0.17)\\
 Fixed Frequency & 1.23 (1.4) & 0.48 (0.51) & 0.24 (0.18)\\
 \hline
\end{tabular}\\

In case of different simulation-types it turns out that there is no significant difference in terms of performance between continuous, dynamic and fixed frequency adaptations, slight differences in detection ratio are being compromised with increased over-fitting hence it is not possible to select a supreme type. On the other hand, there is a significant difference in computation time where the most computationally intensive type that is continuous adaptation took in case on AAPL tweets dataset on average 7.18 times longer than the fastest fixed frequency type, with dynamic type taking 1.5 times longer than fixed frequency counterpart.
\subsubsection{\textnu{} parameter}
\includegraphics[scale=0.20]{graphics/indepth_param_vis/nu_indepth_vis.png}

\begin{tabular}{ |p{1.6cm}||p{1.6cm}|p{1.6cm}|p{1.6cm}|  }
 \hline
 \multicolumn{4}{|c|}{Performance Based on \textnu{} value }\\
 \multicolumn{4}{|c|}{for AAPL Tweets (and NYC Taxi)} \\
 \hline
 \textnu{} & Avg. Indepth Det. Ratio To Baseline & Avg. Indepth Det. Ratio & Avg. Over-fitting Ratio  \\
 \hline
 0.01 & 1.3 (1.29) & 0.39 (0.47) & 0.18 (0.17)\\
 0.05 & 1.34 (1.31) & 0.34 (0.39) & 0.19 (0.17)\\
 0.1 & 1.31 (1.42) & 0.49 (0.52) & 0.22 (0.19)\\
 \hline
\end{tabular}\\

When analysing effect of different \textnu{} values on the outliers detection performance, once again it can be observed that the performance ratios are very similar. In case of low \textnu{} value which in theory should make the model classify only around 1\% of the dataset as outliers, tends to have as high over-fitting ratios as the other configurations. That disadvantage might originate from the fact that the amount of data-points used to train a model was not large enough for OCSVM to create a meaningful decision criteria when so few datapoints were allowed to be classified as outliers, leading to the conclusion  that overall the OCSVM method might not perform well enough with times-series where outliers are rarely occurring.
\subsubsection{Time-series folding dimensionality and lag}
\includegraphics[scale=0.20]{graphics/indepth_param_vis/dimension_indepth_vis.png}

\begin{tabular}{ |p{1.6cm}||p{1.6cm}|p{1.6cm}|p{1.6cm}|  }
 \hline
 \multicolumn{4}{|c|}{Performance Based on num. of dimensions }\\
 \multicolumn{4}{|c|}{for AAPL Tweets (and NYC Taxi)} \\
 \hline
 Num. Of Dim. & Avg. Indepth Det. Ratio To Baseline & Avg. Indepth Det. Ratio & Avg. Over-fitting Ratio  \\
 \hline
 1 & 0.68 (0.98) & 0.15 (0.18) & 0.15 (0.15)\\
 2 & 1.95 (1.48) & 0.50 (0.58) & 0.18 (0.15)\\
 3 & 1.40 (1.63) & 0.54 (0.60) & 0.22 (0.18)\\
 4 & 1.24 (1.22) & 0.55 (0.61) & 0.24 (0.22)\\
 \hline
\end{tabular}\\

Another independent variables tweaked to observe impact on overall performance were integral parts of time-series folding criteria that is dimensions and lag. Analysing detection results based on dimensionality of vector's data-points it is apparent that the time-series folding is indeed necessary outlier detection to perform well. The most basic one dimensional configurations which tries to detect an outlier based only on its current measurement's value had significantly under-performed when compared to two, three and four dimensional configurations. On average its detection ratio was less than a third of the higher-dimensional configurations' ratios. For higher dimensional configurations the results tend to be very similar. Slight advantage might be observed in two dimensional configurations, which while detecting similar amount of artificially generated outliers manage to sustain lower over-fitting ratio. A reason for which higher dimensional models classify more data-points as outliers can originate from the situation where with more measurements within data-point vector, more internal noise appear which results in a confusion of detection's model worsens the quality of its predictions.\\
\includegraphics[scale=0.20]{graphics/indepth_param_vis/lag_indepth_vis.png}

\begin{tabular}{ |p{1.6cm}||p{1.6cm}|p{1.6cm}|p{1.6cm}|  }
 \hline
 \multicolumn{4}{|c|}{Performance Based on val. of delay }\\
 \multicolumn{4}{|c|}{for AAPL Tweets (and NYC Taxi)} \\
 \hline
 Delay Value & Avg. Indepth Det. Ratio To Baseline & Avg. Indepth Det. Ratio & Avg. Over-fitting Ratio  \\
 \hline
 1 & 0.63 (0.56) & 0.49 (0.57) & 0.15 (0.12)\\
 2 & 2.05 (2.63) & 0.49 (0.57) & 0.18 (0.16)\\
 5 & 1.27 (0.80) & 0.34 (0.35) & 0.26 (0.25)\\
 \hline
\end{tabular}\\

Focusing on the second parameter that is the delay of data-point's vector's elements opposite correlation could have been observed. The highest tested delay of 5 was on average 30\% less efficient as other configurations with delay of one or two. On the other hand higher delay meant that there were fewer data-points used as a training data for the model since the amount of train data can be expressed by the fraction of sliding window size and the delay value. That meant that for configurations with higher delay value each retrain took less time. That effect is reflected in the results processing-time. The configurations with delay value of five processed two times faster than its counterparts with lower or no delay.
\subsubsection{Sliding Window Size}
\includegraphics[scale=0.20]{graphics/indepth_param_vis/sliding_window_size_indepth_vis.png}

\begin{tabular}{ |p{1.6cm}||p{1.6cm}|p{1.6cm}|p{1.6cm}|  }
 \hline
 \multicolumn{4}{|c|}{Performance Based on sliding win. size }\\
 \multicolumn{4}{|c|}{for AAPL Tweets (and NYC Taxi)} \\
 \hline
 Sliding Window Size & Avg. Indepth Det. Ratio To Baseline & Avg. Indepth Det. Ratio & Avg. Over-fitting Ratio  \\
 \hline
 72 (48)    & 0.91 (0.72) & 0.68 (0.69) & 0.39 (0.37)\\
 288 (336)  & 1.51 (1.48) & 0.45 (0.46) & 0.16 (0.12)\\
 2016 (672) & 1.59 (1.73) & 0.23 (0.42) & 0.07 (0.10)\\
 \hline
\end{tabular}\\

Last parameter considered within this study relates to the sliding window approach used to feed-in time-series data to machine learning models. Tweaked parameters cover sliding window size itself which in example of data-sets used ranges between 0.4\% and 12\% of the whole dataset sample. When analysing the result it is visible that the performance of predictions (understood as ratio of in-depth detection and number of all detected outliers) increases with enlargement of a sliding window size. That dependency however, is following a logarithmic curve meaning that at the beginning a small increase in the window size can significantly increase the performance when further increases contribute only to a small improvement in performance. Drilling down into individual performance benchmarks it can be observed that small sliding windows lead to highest number of detected outliers, which, however, gets diluted in the highest overall number of data-points classified as outliers. Results like this can be explained with assumption that small sliding windows shrinks amount of data used to train the detection model, resulting in most of the new points being incorrectly classified as outliers. Exactly opposite behaviour can be seen as the sliding window gets larger. Smaller fraction of artificial outliers gets detected by the model but in the same time most of the incoming data-points are being correctly classified as true negatives As the side note, similar pattern could have been observed with fixed frequency time-series processing adaptation and the predefined time between model's retrains. Again with increased time-frame the overall performance improves since the model becomes less sensitive and even though overall less artificial outliers gets detected the final results benefits by avoiding the excessive over-fitting.

\section{Conclusions}
To be added after PCA

\bibliographystyle{abbrv}

\end{document}
