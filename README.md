## Codebase of dissertation project by Pawel Heldt (2268686h)
# _On the use of Unsupervised Learning for Online OutliersDetection in Multivariate Data Streams_

## Codebase describtion

- code/main - entry point to run the simulation of outlier detection with PCA and OCSVM
- data/NAB - directory containing original datasets, datasets with artificial outliers added, analysis containing results, simulation containing gifs of the classification process

