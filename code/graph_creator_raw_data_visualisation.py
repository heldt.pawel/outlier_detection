import os
import matplotlib.pyplot as plt
from util import get_values
from graph_creator import GraphCreator

class GraphCreatorRawDataVisualisation(GraphCreator):
    def __init__(self):
        super().__init__()

    def create_2d_graph(self, ax, x_desc, y_desc, x_axis, y_axis):
        ax.set_xlabel(x_desc)
        ax.set_ylabel(y_desc)
        ax.plot(x_axis, y_axis, 'bo')

    def create_3d_graph(self, ax, x_desc, y_desc, z_desc, x_axis, y_axis, z_axis):
        ax.scatter(x_axis, y_axis, z_axis, c='r', marker='o')
        ax.set_xlabel(x_desc)
        ax.set_ylabel(y_desc)
        ax.set_zlabel(z_desc)

    def create_graph(self, root_dir, df_name, df):
        r, c = 5, 2
        values = get_values(df)
        self.fig.set_figheight(40)
        self.fig.set_figwidth(16)
        self.fig.suptitle("{}/{}".format(root_dir, df_name))

        ax1 = self.fig.add_subplot(r, c, 1, title="2d, No Lag, K=1")
        self.create_2d_graph(ax1, "t[i]", "t[i-1]", values[1:], values[:-1])

        ax2 = self.fig.add_subplot(r, c, 2, projection='3d', title="3d, No Lag, K=1")
        self.create_3d_graph(ax2, "t[i]", "t[i-1]", "t[i-2]", values[2:], values[1:-1], values[:-2])

        ax3 = self.fig.add_subplot(r, c, 3, title="2d, K = 2")
        self.create_2d_graph(ax3, "t[i]", "t[i-2]", values[2::2], values[:-2:2])

        ax4 = self.fig.add_subplot(r, c, 4, projection='3d', title="3d, K=2")
        self.create_3d_graph(ax4, "t[i]", "t[i-2]", "t[i-4]", values[4::2], values[2:-2:2], values[:-4:2])

        ax5 = self.fig.add_subplot(r, c, 5, title="2d, K = 3")
        self.create_2d_graph(ax5, "t[i]", "t[i-3]", values[3::3], values[:-3:3])

        ax6 = self.fig.add_subplot(r, c, 6, projection='3d', title="3d, K=3")
        self.create_3d_graph(ax6, "t[i]", "t[i-3]", "t[i-6]", values[6::3], values[3:-3:3], values[:-6:3])

        ax7 = self.fig.add_subplot(r, c, 7, title="2d, K = 10")
        self.create_2d_graph(ax7, "t[i]", "t[i-10]", values[10::10], values[:-10:10])

        ax8 = self.fig.add_subplot(r, c, 8, projection='3d', title="3d, K=10")
        self.create_3d_graph(ax8, "t[i]", "t[i-10]", "t[i-20]", values[20::10], values[10:-10:10], values[:-20:10])

        ax9 = self.fig.add_subplot(r, c, 9, title="2d, K = 20")
        self.create_2d_graph(ax9, "t[i]", "t[i-20]", values[20::20], values[:-20:20])

        ax10 = self.fig.add_subplot(r, c, 10, projection='3d', title="3d, K=20")
        self.create_3d_graph(ax10, "t[i]", "t[i-20]", "t[i-40]", values[40::20], values[20:-20:20], values[:-40:20])


    def create_graph_with_artificial_outliers(self, root_dir, df_name, df, df_mod, outlier_indexes):
        values_origin = get_values(df)
        values_mod = get_values(df_mod)
        mod_y_ax = []
        mod_x_ax = []
        shadows_y_ax = []
        shadows_x_ax = []

        for ind in outlier_indexes:
            mod_y_ax.append(values_mod[ind-1])
            mod_x_ax.append(values_mod[ind])
            shadows_y_ax.append(values_mod[ind])
            shadows_x_ax.append(values_mod[ind+1])

        r, c = 1, 1
        self.fig.set_figheight(10)
        self.fig.set_figwidth(10)
        self.fig.suptitle("{}/{} with artificially generated outliers".format(root_dir, df_name))

        ax1 = self.fig.add_subplot(r, c, 1, title="{} artificially generated outliers over {} data-points".format(len(outlier_indexes), len(df)-len(outlier_indexes)))
        ax1.set_xlabel("t[i]")
        ax1.set_ylabel("t[i-1]")
        ax1.plot(values_origin[1:], values_origin[:-1], 'bo', markersize=4)
        ax1.plot(mod_x_ax, mod_y_ax, 'ro', markersize=4)
        ax1.plot(shadows_x_ax, shadows_y_ax, 'go', markersize=4)

        # ax2 = self.fig.add_subplot(r, c, 2, projection='3d', title="3d, No Lag, K=1")
        # self.create_3d_graph(ax2, "t[i]", "t[i-1]", "t[i-2]", values[2:], values[1:-1], values[:-2])

    def save_graph(self, root_path, cur_dir, graph_name, outliers=False):
        path = "{}/graphs/{}".format(root_path, cur_dir)
        if not os.path.isdir(path):
            os.makedirs(path)
        if outliers:
            self.fig.savefig(os.path.join(path, "{}_outliers.png".format(graph_name)))
        else:
            self.fig.savefig(os.path.join(path, "{}.png".format(graph_name)))
