from sklearn import svm
from sklearn.model_selection import train_test_split
from util import get_values, create_dim
import numpy as np

class OneClassSVM(object):
    def __init__(self, nu=0.1, gamma="scale", lag=1, nD=2):
        self.clf = svm.OneClassSVM(nu=nu, kernel="rbf", gamma=gamma)
        self.lag = lag
        self.nD = nD

    def model_fit_predict(self, df_values, test_size=0.2):
        values = create_dim(get_values(df_values), self.lag, self.nD)
        val_train, val_test = train_test_split(values, test_size=test_size)
        self.clf.fit(val_train)
        return self.clf.predict(val_train), self.clf.predict(val_test)

    def model_fit_predict_only_custom_data(self, df_slice):
        val_train = create_dim(get_values(df_slice), self.lag, self.nD)
        self.clf.fit(val_train)
        return self.clf.predict(val_train)

    def model_latest_datapoint_prediction(self, df_slice):
        latest_datapoint = create_dim(get_values(df_slice), self.lag, self.nD)[-1]
        return self.clf.predict(latest_datapoint.reshape(1, latest_datapoint.shape[0]))