import os
import pandas as pd
import json
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
import matplotlib

def analyse_results(dir, method="OCSVM"):
    path = "/home/pawel/Uni/Dissertation/data/NAB/analysis_{}/{}_simulation_comparison".format(method, method.lower())

    df = pd.read_excel("{}/{}/{}_performance_overview.xlsx".format(path, dir, dir))

    agg_stat(dir, df, method)

    best_performers_overall_naive = df[df["simulation_type"] != "Baseline"].nlargest(10, columns=["naive_artificial_outlier_detection_ratio"])
    best_performers_overall_indepth = df[df["simulation_type"] != "Baseline"].nlargest(10, columns=["indepth_artificial_outlier_detection_ratio"])

    best_performers_3xnu_naive = df.query("simulation_type != \"Baseline\" and all_outliers_ratio <= 3*nu").nlargest(10, columns=["naive_artificial_outlier_detection_ratio"])
    best_performers_3xnu_indepth = df.query("simulation_type != \"Baseline\" and all_outliers_ratio <= 3*nu").nlargest(10, columns=["indepth_artificial_outlier_detection_ratio"])

    best_performers_1xnu_naive = df.query("simulation_type != \"Baseline\" and all_outliers_ratio <= 1.5 * nu").nlargest(10, columns=["naive_artificial_outlier_detection_ratio"])
    best_performers_1xnu_indepth = df.query("simulation_type != \"Baseline\" and all_outliers_ratio <= 1.5 * nu").nlargest(10, columns=["indepth_artificial_outlier_detection_ratio"])

    sorting_params = ["simulation_type", "nu",	"dimension", "lag", "sliding_window_size", "fixed_frequency"]

    params_results_indepth = {}
    params_results_naive = {}
    for p in sorting_params:
        df_grouped = df.groupby(by=[p])["indepth_artificial_outlier_detection_ratio"].mean()
        params_results_indepth[p] = df_grouped.to_dict()
        df_grouped = df.groupby(by=[p])["naive_artificial_outlier_detection_ratio"].mean()
        params_results_naive[p] = df_grouped.to_dict()

    with pd.ExcelWriter("{}/{}/best_performers.xlsx".format(path, dir)) as writer:
        best_performers_overall_indepth.to_excel(writer, "overall_indepth_{}".format(dir))
        best_performers_overall_naive.to_excel(writer, "overall_naive_{}".format(dir))

        best_performers_3xnu_indepth.to_excel(writer, "3xnu_inepth_{}".format(dir))
        best_performers_3xnu_naive.to_excel(writer, "3xnu_naive_{}".format(dir))

        best_performers_1xnu_indepth.to_excel(writer, "1xnu_indepth_{}".format(dir))
        best_performers_1xnu_naive.to_excel(writer, "1xnu_naive_{}".format(dir))
        writer.save()

    with open("{}/{}/param_results.txt".format(path, dir), "w") as f:
        f.write("indepth breakdown\n")
        f.write(json.dumps(params_results_indepth))
        f.write("\n\n\n\nnaive breakdown\n")
        f.write(json.dumps(params_results_indepth))

def visualise_results(dir):
    path = "/home/pawel/Uni/Dissertation/data/NAB/analysis_OCSVM/ocsvm_simulation_comparison"

    filename =  "NYC_Taxi" if dir == "realKnowCause" else "AAPL_Tweets"
    df = pd.read_excel("{}/{}/{}_performance_overview.xlsx".format(path, dir, dir))

    sorting_params = ["simulation_type", "nu", "dimension", "lag", "sliding_window_size", "fixed_frequency"]

    df = df[(np.isfinite(df["naive_to_baseline_ratio"])) & (np.isfinite(df["indepth_to_baseline_ratio"]))]

    for p in sorting_params:
        data_naive = []
        data_indepth = []
        for p_value in df[p].unique():
            if p_value == 'Baseline':
                continue
            data_naive.append((p_value, df[(df[p]==p_value) & (df.simulation_type!="Baseline")].naive_to_baseline_ratio))
            data_indepth.append((p_value, df[(df[p]==p_value) & (df.simulation_type!="Baseline")].indepth_to_baseline_ratio))

        SMALL_SIZE = 24
        MEDIUM_SIZE = 34
        BIGGER_SIZE = 36

        plt.rc('font', size=BIGGER_SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
        plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
        plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

        fig = plt.figure()
        fig.set_figheight(10)
        fig.set_figwidth(20)
        fig.suptitle("Breakdown of performances for different values of {} in {}".format(p, filename))

        ax1 = fig.add_subplot(1, 2, 1, title="Naive Ratio")
        ax1.set_xlabel("{} values".format(p))
        ax1.set_ylabel("Score")
        ax1.boxplot([tuple[1] for tuple in data_naive])
        ax1.set_xticklabels(["{}-\n{} obs.".format(tuple[0], len(tuple[1])) for tuple in data_indepth])
        ax1.set_ylim([0, 10])

        ax2 = fig.add_subplot(1, 2, 2, title="Indepth Ratio")
        ax2.set_xlabel("{} values".format(p))
        ax2.set_ylabel("Score")
        ax2.boxplot([tuple[1] for tuple in data_indepth])
        ax2.set_xticklabels(["{}-\n{} obs.".format(tuple[0], len(tuple[1])) for tuple in data_indepth])
        ax2.set_ylim(0, 10)

        plt.show()
        fig.savefig("{}/performance_visualisation/{}_score_visualisation.png".format(os.path.join(path, dir), p))


def all_dataset_visualisation(root_path, dir_aapl, dir_nyc, method):
    df_all = pd.DataFrame()
    for dataset in [dir_aapl, dir_nyc]:
        path = "{}/analysis_{}/{}_simulation_comparison/{}".format(root_path, method, method.lower(), dataset)
        excel = pd.ExcelFile("{}/{}_performance_overview.xlsx".format(path, dataset))
        performances = {}
        for sheet_name in excel.sheet_names:
            performances[sheet_name] = excel.parse(sheet_name)

        for name, df in performances.items():
            df.drop(['Unnamed: 0'], axis=1)
            df = df.fillna('-1')
            df['dataset'] = "AAPL_Tweets" if dataset == dir_aapl else "NYC_Taxi"
        df_all = pd.concat([df_all, df])

    df_all = df_all[ df_all["simulation_type"] != "Baseline"]

    SMALL_SIZE = 24
    MEDIUM_SIZE = 34
    BIGGER_SIZE = 30

    plt.rc('font', size=BIGGER_SIZE)  # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=MEDIUM_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


    fig = plt.figure(figsize=(16, 16))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title("Overview of configuration performances in {} classification\n by different benchmark criteria".format(method))
    ax.set_ylabel("Accuracy")
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 1])
    ax.set_xticks(np.arange(0, 1, 0.1))
    ax.set_yticks(np.arange(0, 1, 0.1))
    colours = {"AAPL_Tweets": ["#FF2D00"], "NYC_Taxi": ["#0017FF"]}

    for dataset_ver, rows1 in df_all.groupby(['dataset']):
        rows1.plot.scatter('all_outliers_ratio', 'naive_artificial_outlier_detection_ratio', ax=ax, color=colours.get(dataset_ver).pop(), marker='^')

    ax.set_ylabel("Accuracy")
    ax.set_xlabel("Over-fitting Ratio")

    leg_vis = [Line2D([0], [0], marker='^', color='w', markerfacecolor='#0017FF', label='Scatter', markersize=22),
               Line2D([0], [0], marker='v', color='w', markerfacecolor='#FF2D00', label='Scatter', markersize=22),
               ]

    leg_desc = ["NYC_Taxis Ratio", "AAPL_Tweets Ratio"]
    ax.legend(leg_vis, leg_desc)

    dest_path = "{}/analysis_{}/{}_simulation_comparison/both_datasets_performance_visualisation".format(root_path, method, method.lower())
    if not os.path.isdir(dest_path):
        os.mkdir(dest_path)
    plt.savefig(os.path.join(dest_path, "detection_comparison.png"))
    plt.close()
    print("4")


def performance_visualisation(root_path, dir_aapl, dir_nyc, method="OCSVM"):
    df_all = pd.DataFrame()
    for dataset in [dir_aapl, dir_nyc]:
        path = "{}/analysis_{}/{}_simulation_comparison/{}".format(root_path, method, method.lower(), dataset)
        excel = pd.ExcelFile("{}/{}_performance_overview.xlsx".format(path, dataset))
        performances = {}
        for sheet_name in excel.sheet_names:
            performances[sheet_name] = excel.parse(sheet_name)

        for name, df in performances.items():
            df.drop(['Unnamed: 0'], axis=1)
            df = df.fillna('-1')
            df['dataset'] = "AAPL_Tweets" if dataset == dir_aapl else "NYC_Taxi"
        df_all = pd.concat([df_all, df])

    df_all.loc[df_all.simulation_type == "Baseline", "dataset"] += "_baseline"

    matplotlib.rcParams.update({'font.size': 22})

    colours = ["#0016ff", "#7682ff", "#ff0000", "#ff7070", "#00970b", "#73dc7a", "#8e00fd", "#bf90e3"]  # blue, pale blue, red, pale red, green pale green, violet, pale violet
    colours_baseline = ["#000000", "#747474"]
    markers = ["o", "s", "v", "P"]
    params = ["simulation_type", "nu", "dimension",	"lag", "sliding_window_size"]
    if method == "PCA":
        params.append("n_component")
    for param in params :
        leg_vis = []
        leg_desc = []

        SMALL_SIZE = 24
        MEDIUM_SIZE = 28
        BIGGER_SIZE = 32

        plt.rc('font', size=BIGGER_SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
        plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
        plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

        fig = plt.figure(figsize=(16, 16))
        ax = fig.add_subplot(1, 1, 1)

        ax.set_ylim([0, 1])
        ax.set_xlim([0, 1])
        ax.set_xticks(np.arange(0, 1, 0.1))
        ax.set_yticks(np.arange(0, 1, 0.1))
        df_baseline = df_all[df_all["dataset"].str.contains("_baseline")]
        i = 0
        for param_val, rows1 in df_baseline.groupby([param]):
            for dataset_ver, rows2 in rows1.groupby(['dataset']):
                rows2.plot.scatter('all_outliers_ratio', 'naive_artificial_outlier_detection_ratio', ax=ax,
                               color=colours_baseline[i%2], marker=markers[i%4], s=30)
                if param == "sliding_window_size":
                    leg_desc.append("{} N/A {}".format(dataset_ver, param))
                else:
                    leg_desc.append("{} {} {}".format(dataset_ver, param_val, param))
                leg_vis.append(Line2D([0], [0], marker=markers[i%4], color='w', markerfacecolor=colours_baseline[i%2], label='Scatter', markersize=22))
                i+=1

        df_no_baseline = df_all[~df_all["dataset"].str.contains("_baseline")]
        df_fixed_freq_aapl = df_no_baseline[(df_no_baseline["simulation_type"]=="fixed_freq") & (df_no_baseline["dataset"]=="AAPL_Tweets")].sort_values(by=["naive_artificial_outlier_detection_ratio"]).iloc[::5, :]
        df_fixed_freq_nyc = df_no_baseline[(df_no_baseline["simulation_type"]=="fixed_freq") & (df_no_baseline["dataset"]=="NYC_Taxi")].sort_values(by=["naive_artificial_outlier_detection_ratio"]).iloc[::5, :]
        df_no_baseline = df_no_baseline[df_no_baseline["simulation_type"]!="fixed_freq"]
        df_no_baseline = pd.concat([df_no_baseline, df_fixed_freq_aapl, df_fixed_freq_nyc])
        i=0
        for param_val, rows1 in df_no_baseline.groupby([param]):
            for dataset_ver, rows2 in rows1.groupby(['dataset']):
                rows2.plot.scatter('all_outliers_ratio', 'naive_artificial_outlier_detection_ratio', ax=ax,  color=colours[i], marker=markers[i%4], s=30)
                leg_desc.append("{} {} {}".format(dataset_ver, param_val, param))
                leg_vis.append(
                    Line2D([0], [0], marker=markers[i % 4], color='w', markerfacecolor=colours[i],
                           label='Scatter', markersize=22))
                i += 1

        ax.legend(leg_vis, leg_desc)
        ax.set_title(
            "Overview of configuration performances in {} classification\naggregated on {} parameter".format(method,
                                                                                                             param))
        ax.set_ylabel("Accuracy")
        ax.set_xlabel("Over-fitting ratio")
        dest_path = "{}/analysis_{}/{}_simulation_comparison/both_datasets_performance_visualisation".format(
            root_path, method, method.lower())
        if not os.path.isdir(dest_path):
            os.mkdir(dest_path)
        plt.savefig(os.path.join(dest_path, "{}_naive_vis.png".format(param)))
        plt.close()
        print("4")



def agg_stat(dir, df, method):
    # Get Average Naive Ratio
    avg_naive = df[df["naive_to_baseline_ratio"] != "Baseline"]["naive_to_baseline_ratio"].mean()
    median_naive = df[df["naive_to_baseline_ratio"] != "Baseline"]["naive_to_baseline_ratio"].median()
    # Get Average Indepth Ratio
    avg_indepth = df[df["indepth_to_baseline_ratio"] != "Baseline"]["indepth_to_baseline_ratio"].mean()
    median_indepth = df[df["indepth_to_baseline_ratio"] != "Baseline"]["indepth_to_baseline_ratio"].median()
    #
    print("For {} average naive is {} median naive is {} average indepth is {} median indepth is {}".format(dir,
                                                                                                            avg_naive,
                                                                                                            median_naive,
                                                                                                            avg_indepth,
                                                                                                            median_indepth))
     # Get Low Overfit Average Naive Ratio
    df_low_overfit = df.copy().query("simulation_type != \"Baseline\" and all_outliers_ratio <= 1.5 * nu")
    avg_naive = df_low_overfit["naive_to_baseline_ratio"].mean()
    median_naive = df_low_overfit["naive_to_baseline_ratio"].median()
    # Get Low OverfitAverage Indepth Ratio
    avg_indepth = df_low_overfit["indepth_to_baseline_ratio"].mean()
    median_indepth = df_low_overfit["indepth_to_baseline_ratio"].median()

    print("LOW OVERFIT For {} average naive is {} median naive is {} average indepth is {} median indepth is {}".format(dir,
    avg_naive,
    median_naive,
    avg_indepth,
    median_indepth))

    if method == "PCA":
        df = df[df.dimension!=1]
     # # Average Time
    for sim_type in df.simulation_type.unique():
        if sim_type == "Baseline":
            continue
        print("In {} for sim_type {} avg time is {} median time is {} avg overfit is {} median overfit is {} avearge detection ratio is {} median is {}, Naive To Baseline Ratio is {}".format(dir,
                                                                                                                     sim_type,
                                                                                                                     df[df["simulation_type"]==sim_type]["time"].mean(),
                                                                                                                     df[df["simulation_type"]==sim_type]["time"].median(),
                                                                                                                     df[df["simulation_type"]==sim_type]["all_outliers_ratio"].mean(),
                                                                                                                     df[df["simulation_type"]==sim_type]["all_outliers_ratio"].median(),
                                                                                                                     df[df["simulation_type"]==sim_type]["naive_artificial_outlier_detection_ratio"].mean(),
                                                                                                                     df[df["simulation_type"]==sim_type]["naive_artificial_outlier_detection_ratio"].median(),
                                                                                                                     df[df["simulation_type"]==sim_type]["naive_to_baseline_ratio"].mean(),))

    df_no_baseline = df[df["simulation_type"] != "Baseline"]
    # # Average NU
    for nu_val in df_no_baseline.nu.unique():
        print("In {} for nu {} avg overfit is {} !!!! avearge detection ratio is {} !!!! naive ratio is {}".format(
                dir,
                nu_val,
                df[df["nu"] == nu_val]["all_outliers_ratio"].mean(),
                df[df["nu"] == nu_val]["naive_artificial_outlier_detection_ratio"].mean(),
                df[df["nu"] == nu_val]["naive_to_baseline_ratio"].mean()))

    # average nd
    for nd in df_no_baseline.dimension.unique():
        print("In {} for nd {} avg overfit is {} !!!! avearge detection ratio is {} !!!! naivenaive ratio is {}".format(
            dir,
            nd,
            df[df["dimension"] == nd]["all_outliers_ratio"].mean(),
            df[df["dimension"] == nd]["naive_artificial_outlier_detection_ratio"].mean(),
            df[df["dimension"] == nd]["naive_to_baseline_ratio"].mean()))
    # average lag
    for lag in df_no_baseline.lag.unique():
        print("In {} for lag {} avg overfit is {} !!!! avearge detection ratio is {} !!!! naive ratio is {} avg time is {}".format(
            dir, lag, df[df["lag"] == lag]["all_outliers_ratio"].mean(),
            df[df["lag"] == lag]["naive_artificial_outlier_detection_ratio"].mean(),
            df[df["lag"] == lag]["naive_to_baseline_ratio"].mean(),
            df[df["lag"] == lag]["naive_to_baseline_ratio"].mean()))
   #average sliding window size
    for win_size in df_no_baseline.sliding_window_size.unique():
        print("In {} for win_size {} avg overfit is {} !!!! avearge detection ratio is {} !!!! naivenaive ratio is {}".format(
            dir, win_size, df[df["sliding_window_size"]==win_size]["all_outliers_ratio"].mean(),
            df[df["sliding_window_size"]==win_size]["naive_artificial_outlier_detection_ratio"].mean(),
            df[df["sliding_window_size"]==win_size]["naive_to_baseline_ratio"].mean()))

     #fixed freq overview
    for fixed_freq in df_no_baseline.fixed_frequency.unique():
        print(
            "In {} for fixed_freq {} avg overfit is {} !!!! avearge detection ratio is {} !!!! naivenaive ratio is {}".format(
                dir, fixed_freq, df[df["fixed_frequency"]==fixed_freq]["all_outliers_ratio"].mean(),
                df[df["fixed_frequency"]==fixed_freq]["naive_artificial_outlier_detection_ratio"].mean(),
                df[df["fixed_frequency"]==fixed_freq]["naive_to_baseline_ratio"].mean()))

    #n_component
    if method == "PCA":
        for n_comp in df_no_baseline.n_component.unique():
            print(
                "In {} for n_component {} avg overfit is {} !!!! avearge detection ratio is {} !!!! naivenaive ratio is {}".format(
                    dir, n_comp, df[df["n_component"]==n_comp]["all_outliers_ratio"].mean(),
                    df[df["n_component"]==n_comp]["naive_artificial_outlier_detection_ratio"].mean(),
                    df[df["n_component"]==n_comp]["naive_to_baseline_ratio"].mean()))
