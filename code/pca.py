from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from util import get_values, create_dim
import numpy as np
from sklearn.preprocessing import StandardScaler

class PCA_obj(object):
    def __init__(self, n_component=2, nu=0.1, lag=1, nD=2):
        self.clf = PCA(n_components=n_component)
        self.n_component = n_component
        self.nu = nu
        self.lag = lag
        self.nD = nD
        self.sliding_window_slice = None
        self.sliding_window_slice_end_index = None

    def model_fit_data(self, df_slice):
        self.sliding_window_slice = create_dim(get_values(df_slice), self.lag, self.nD)
        self.sliding_window_slice_start_index = df_slice.index[-1]-(len(self.sliding_window_slice)-1)*self.lag
        self.clf.fit(self.sliding_window_slice)

    def model_transform_inverse_data(self, df_slice):
        latest_datapoint = create_dim(get_values(df_slice), self.lag, self.nD)[-1]
        values_transformed = self.clf.transform(np.vstack([self.sliding_window_slice, latest_datapoint]))
        values_inverse_transformed = self.clf.inverse_transform(values_transformed)
        MSE_score = ((np.vstack([self.sliding_window_slice, latest_datapoint])-values_inverse_transformed)**2).sum(axis=1)
        outlier_indexes_relative = MSE_score.argsort()[int(len(MSE_score)*(1-self.nu)):]
        outlier_indexes_absolute = [(index * self.lag) + self.sliding_window_slice_start_index if index != len(MSE_score)-1 else df_slice.index[-1] for index in outlier_indexes_relative]
        return outlier_indexes_absolute

    def model_fit_transform_inverse_data(self, df_slice):
        values = create_dim(get_values(df_slice), self.lag, self.nD)
        values_transformed = self.clf.fit_transform(values)
        values_inverse_transformed = self.clf.inverse_transform(values_transformed)
        MSE_score = ((values-values_inverse_transformed)**2).sum(axis=1)
        outlier_indexes_relative = MSE_score.argsort()[int(len(MSE_score)*(1-self.nu)):]

        start_index = df_slice.index[-1]-(len(values)-1)*self.lag
        outlier_indexes_absolute = [(index * self.lag) + start_index for index in outlier_indexes_relative]
        return outlier_indexes_absolute