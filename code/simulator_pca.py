import matplotlib
import matplotlib.pyplot as plt
from util import *
import os
import imageio
from pygifsicle import optimize
from pca import PCA_obj
import numpy as np
from skimage import measure
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.patches as mpatches
import matplotlib.font_manager
import shutil
from datetime import datetime

class SimulatorPCA(object):
    def __init__(self, root_path, cur_dir, df_name, df, sliding_window_size=1000, naive_outlier_indexes=[], indepth_outlier_indexes=[], visualisation=False, n_component=1):
        self.root_path = root_path
        self.cur_dir = cur_dir
        self.sliding_window_size = sliding_window_size
        self.df_name = df_name
        self.df = df
        self.novelty_ct = [0]
        self.inlier_ct = [0]
        self.redundant_retrain = 0
        self.naive_outlier_indexes = naive_outlier_indexes
        self.naive_outlier_ct = []
        self.indepth_outlier_indexes = indepth_outlier_indexes
        self.indepth_outlier_ct = []
        self.repeated_counts = {}
        self.visualisation = visualisation
        self.n_component = n_component

        ###only for dynamic adaptation
        self.dynamic_actual_outlier = 0

    def simulate_continuous(self, lag, nD, nu=0.1):
        # set-up
        self._reset_obj_params()
        ts_pos = self.sliding_window_size

        # routine start
        pca = PCA_obj(n_component=self.n_component, nu=nu, lag=lag, nD=nD)
        sliding_window_df = self.df[:ts_pos]
        pca.model_fit_data(sliding_window_df)
        while ts_pos < len(self.df) - 1:
            self._log_ts_pos(ts_pos)

            ts_pos += 1
            sliding_window_df = self.df[ts_pos + 2 - self.sliding_window_size - 1:ts_pos+1]
            pred_outliers = pca.model_transform_inverse_data(sliding_window_df)  # check if next elem is an inlier
            self._check_if_outlier(ts_pos, pred_outliers, nD, lag)
            pca.model_fit_data(sliding_window_df)
            self._visualise_classification( nD, lag, ts_pos, sliding_window_df, pred_outliers)

        if self.visualisation:
            self.create_gif_of_clf_process(nD, lag, sim_type="continuous")


    def simulate_fixed_freq(self, fixed_freq, lag, nD, nu=0.1):
        # set-up
        self._reset_obj_params()
        ts_pos = self.sliding_window_size

        # routine start
        pca = PCA_obj(n_component=self.n_component, nu=nu, lag=lag, nD=nD)
        sliding_window_df = self.df[ts_pos - self.sliding_window_size:ts_pos]
        while ts_pos < len(self.df) - 1:
            self._log_ts_pos(ts_pos)
            if ts_pos % fixed_freq == 0 or ts_pos == self.sliding_window_size:
                pca.model_fit_data(sliding_window_df)
            ts_pos += 1
            sliding_window_df = self.df[ts_pos + 2 - self.sliding_window_size:ts_pos + 1]
            pred_outliers = pca.model_transform_inverse_data(sliding_window_df)
            self._check_if_outlier(ts_pos, pred_outliers, nD, lag)
            self._visualise_classification(nD, lag, ts_pos, sliding_window_df, pred_outliers)

        if self.visualisation:
            self.create_gif_of_clf_process(nD, lag, sim_type="continuous")

    def simulate_dynamic(self, lag, nD, nu=0.1):
        # set-up
        self._reset_obj_params()
        ts_pos = self.sliding_window_size

        # routine start
        sliding_window_df = self.df[:ts_pos]
        pca = PCA_obj(n_component=self.n_component, nu=nu, lag=lag, nD=nD)
        pca.model_fit_data(sliding_window_df)
        while ts_pos < len(self.df) - 1:
            self._log_ts_pos(ts_pos)
            ts_pos += 1
            sliding_window_df = self.df[ts_pos + 2 - self.sliding_window_size:ts_pos+1]
            pred_outliers = pca.model_transform_inverse_data(sliding_window_df)  # check if next elem is an inlier
            self._check_if_outlier(ts_pos, pred_outliers, nD, lag)
            if ts_pos in pred_outliers:
                pca.model_fit_data(sliding_window_df)
            self._visualise_classification(nD, lag, ts_pos, sliding_window_df, pred_outliers)

        if self.visualisation:
            self.create_gif_of_clf_process(nD, lag, sim_type="continuous")

    def _check_if_outlier(self, ts_pos, pred_outliers, nD, lag):
        if ts_pos in pred_outliers:
            self.novelty_ct.append(self.novelty_ct[-1] + 1)
            self.inlier_ct.append(self.inlier_ct[-1])
            if ts_pos in self.naive_outlier_indexes:
                self.naive_outlier_ct.append(ts_pos)
            if ts_pos in self.indepth_outlier_indexes:
                self.indepth_outlier_ct.append(ts_pos)

                #find indexes of artificial outliers within indexes
                potential_indexes = set(ts_pos - (d*lag) for d in range(nD)) & set(self.naive_outlier_indexes)
                for index in potential_indexes:
                    if index in self.repeated_counts.keys():
                        self.repeated_counts[index] += 1
                    else:
                        self.repeated_counts[index] = 0 #starting with zero to avoid substracting first example later on

        elif ts_pos not in pred_outliers:
            self.novelty_ct.append(self.novelty_ct[-1])
            self.inlier_ct.append(self.inlier_ct[-1] + 1)

    def _visualise_classification(self, nD, lag, ts_pos, sliding_window_df, pred_outliers):
        if self.visualisation:
            if nD == 2:
                self.visualise_pca_classification_2d(nD, lag, ts_pos, sliding_window_df, pred_outliers)

    def visualise_pca_classification_2d(self, nD, lag, ts_pos, sliding_window_df, pred_outliers):
        values = create_dim(get_values(sliding_window_df), lag, nD)
        outliers = []
        inliers = []
        for i in range(len(values)):
            if i in pred_outliers:
                outliers.append(values[i])
            else:
                inliers.append(values[i])
        outliers = np.vstack(outliers)
        inliers = np.vstack(inliers)
        fig = plt.figure()
        fig.set_figheight(6)
        fig.set_figwidth(6)
        ax1 = fig.add_subplot(1, 1, 1, title="{}/{} Novelty Detection, step: {}/{}".format(self.cur_dir[:9], self.df_name, ts_pos, len(self.df)))
        ax1.scatter(outliers[:, 0], outliers[:, 1], c="r", label="outliers")
        ax1.scatter(inliers[:, 0], inliers[:, 1], c="b", label="inliers")
        ax1.set_ylabel("t[i-1]")
        ax1.set_xlabel("t[i]")
        ax1.legend()
        path = "{}/simulation/pca/{}/{}/temp".format(self.root_path, self.cur_dir, self.df_name)
        if not os.path.isdir(path):
            os.makedirs(path)
        plt.savefig(os.path.join(path, "{:07d}.png".format(ts_pos)), dpi=40)
        plt.close()

    def create_gif_of_clf_process(self, nD, lag, sim_type):
        path_to_tmp_images = "{}/simulation/pca/{}/{}/temp".format(self.root_path, self.cur_dir, self.df_name)
        path_to_gif = "{}/simulation/pca/{}/{}/{}D_K_{}_{}_adapt_vis.gif".format(self.root_path, self.cur_dir,
                                                                                          self.df_name, nD, lag, sim_type)
        images = os.listdir(path_to_tmp_images)
        images.sort()

        fps = 20
        with imageio.get_writer(path_to_gif, mode='I', fps=fps) as writer:
            for filename in images:
                image = imageio.imread(os.path.join(path_to_tmp_images, filename))
                writer.append_data(image)
        optimize(path_to_gif)
        shutil.rmtree(path_to_tmp_images)

    def _reset_obj_params(self):
        self.novelty_ct = [0]
        self.inlier_ct = [0]
        self.redundant_retrain = 0
        self.naive_outlier_ct = []
        self.indepth_outlier_ct = []
        self.repeated_counts = {}

    def _log_ts_pos(self, ts_pos):
        if ts_pos % 100 == 0:
            print("{}    {}/{} - {}/{}".format(datetime.now().strftime("%H:%M:%S"), self.cur_dir, self.df_name, ts_pos,
                                               len(self.df)))
