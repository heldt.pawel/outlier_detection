from pca import PCA_obj
from simulator_pca import SimulatorPCA
import os
import json
from const.wrong_param_configs import is_good_param_config
from timeit import default_timer as timer
from util import is_config_banned

def compare_simulation_with_baseline(df, root_path, cur_dir, df_name, outlier_indexes=[], n_components=[1], nu_vals =[0.1], lags=[1], dims=[3], sliding_window_sizes=[1000], fixed_freqs=[50]):
    path = "{}/analysis_PCA/pca_simulation_comparison/{}".format(root_path, cur_dir)
    if not os.path.isdir(path):
        os.makedirs(path)

    if os.path.isfile("{}/analysis.json".format(path)):
        with open("{}/analysis.json".format(path), 'r') as f:
            analysis_json = json.loads(f.read())
    else:
        analysis_json = {}

    if df_name not in analysis_json.keys():
        analysis_json[df_name] = {}

    ### Step 1: Run Baseline
    for nu in nu_vals:
        for n_component in n_components:
            for nD in dims:
                for lag in lags:
                    if nD < n_component:
                        continue
                    base_pca = PCA_obj(n_component=n_component, nu=nu, lag=lag, nD=nD)
                    start = timer()
                    pred_outlier_indexes = base_pca.model_fit_transform_inverse_data(df)
                    end = timer()
                    elapsed = end-start

                    testable_indexes = list(range(len(df)-1, -1, -lag))[::-1]
                    naive_artificial_outlier = set(outlier_indexes) & set(pred_outlier_indexes)

                    #outlier detection looking if oulier was spotted in any vector containg that outlier, no matter on which position it is
                    indepth_outlier_pred = {}
                    for index in outlier_indexes: #collecting every data-points tainted with artificially generated outliers
                        for d in range(0, nD):
                            if index + (d * lag) in pred_outlier_indexes:
                                if indepth_outlier_pred.get(index, None):
                                    indepth_outlier_pred[index] += 1
                                else:
                                    indepth_outlier_pred[index] = 1

                    indepth_artificial_outlier_num = 0
                    repeated_counts = 0
                    for k, v in indepth_outlier_pred.items():
                        indepth_artificial_outlier_num += 1
                        if v != 1:
                            repeated_counts += (v-1)

                    step_base = "\n\nBaseline Run of PCA nu={nu}, {nD}D, K={lag}, Princip_Component={n_component}with classification of the whole TS {cur_dir}/{df_name}\n" \
                                      "\tClassification of {data_len} datapoints ({data_len_including_lag} data-points including lag), {num_of_found_outliers} were classified as outliers, giving the nu={nu_ratio}\n" \
                                      "\tOut of {num_of_gen_outliers} artificially generated outliers, {naive_gen_outliers_found} were naivly detected ({naive_ratio} Ratio)\n" \
                                      "\tOut of {num_of_gen_outliers} artificially generated outliers, {indepth_gen_outliers_found} were indepthly detected ({indepth_ratio} Ratio) (Repeated counts amounted to {repeated_counts})\n" \
                                      "\tThe process took {elapsed}s\n".format(
                        nu=nu,
                        nD=nD,
                        lag=lag,
                        n_component=n_component,
                        cur_dir=cur_dir,
                        df_name=df_name,
                        data_len=len(df),
                        data_len_including_lag=len(testable_indexes),
                        num_of_found_outliers=len(pred_outlier_indexes),
                        nu_ratio=len(pred_outlier_indexes)/len(testable_indexes),
                        num_of_gen_outliers=len(outlier_indexes),
                        naive_gen_outliers_found=len(naive_artificial_outlier),
                        naive_ratio=len(naive_artificial_outlier) / len(set(testable_indexes)&set(outlier_indexes)),
                        indepth_gen_outliers_found=indepth_artificial_outlier_num,
                        indepth_ratio=indepth_artificial_outlier_num / len(set(testable_indexes)&set(outlier_indexes)),
                        repeated_counts=repeated_counts,
                        elapsed=elapsed
                    )

                    if 'baseline' not in analysis_json[df_name].keys():
                        analysis_json[df_name]['baseline'] = {}
                    if nu not in analysis_json[df_name]['baseline'].keys():
                        analysis_json[df_name]['baseline'][nu] = {}
                    if n_component not in analysis_json[df_name]['baseline'][nu].keys():
                        analysis_json[df_name]['baseline'][nu][n_component] = {}
                    if nD not in analysis_json[df_name]['baseline'][nu][n_component].keys():
                        analysis_json[df_name]['baseline'][nu][n_component][nD] = {}
                    if lag not in analysis_json[df_name]['baseline'][nu][n_component][nD].keys():
                        analysis_json[df_name]['baseline'][nu][n_component][nD][lag] = {}

                    analysis_json[df_name]['baseline'][nu][n_component][nD][lag] = {'naive_ratio': len(naive_artificial_outlier) / len(set(testable_indexes)&set(outlier_indexes)),
                                                                       'indepth_ratio': indepth_artificial_outlier_num / len(set(testable_indexes)&set(outlier_indexes)),
                                                                       'repeated_counts': repeated_counts,
                                                                       'all_outliers_ratio': len(pred_outlier_indexes)/len(testable_indexes),
                                                                       'time': elapsed}

                    ### Step2: Run for sliding window adapts
                    for sliding_window_size in sliding_window_sizes:
                        if lag * nD >= sliding_window_size:
                            continue

                        print("\n\n\n\n\nnu={}, dim={}, lag={}, sliding_window={}, n_component={}".format(nu, nD, lag, sliding_window_size, n_component))

                        naive_art_outlier_indexes = [i for i in outlier_indexes if i > sliding_window_size]
                        indepth_art_outlier_indexes = list(set(i + (d * lag) for d in range(nD) for i in outlier_indexes if i > sliding_window_size))
                        sim_pca = SimulatorPCA(root_path, cur_dir, df_name, df, sliding_window_size=sliding_window_size, naive_outlier_indexes=naive_art_outlier_indexes, indepth_outlier_indexes=indepth_art_outlier_indexes, visualisation=False, n_component=n_component)

                        step_sim = "\nSimulation of sliding window adaptations with n_compnents={n_component}nu={nu}, {win_size} window size, K={lag} and {nD} dimensions\n".format(
                            n_component=n_component,
                            nu=nu,
                            win_size=sliding_window_size,
                            lag=lag,
                            nD=nD
                        )

                        if 'simulation' not in analysis_json[df_name].keys():
                            analysis_json[df_name]['simulation'] = {}
                        if n_component not in analysis_json[df_name]['simulation'].keys():
                            analysis_json[df_name]['simulation'][n_component] = {}
                        if nu not in analysis_json[df_name]['simulation'][n_component].keys():
                            analysis_json[df_name]['simulation'][n_component][nu] = {}
                        if nD not in analysis_json[df_name]['simulation'][n_component][nu].keys():
                            analysis_json[df_name]['simulation'][n_component][nu][nD] = {}
                        if lag not in analysis_json[df_name]['simulation'][n_component][nu][nD].keys():
                            analysis_json[df_name]['simulation'][n_component][nu][nD][lag] = {}
                        if sliding_window_size not in analysis_json[df_name]['simulation'][n_component][nu][nD][lag]:
                            analysis_json[df_name]['simulation'][n_component][nu][nD][lag][sliding_window_size] = {'continuous': {}, 'fixed_freq': {}, 'dynamic': {}}

                        ###2.1 Continuous adapt
                        step_sim_cont = ""
                        if is_good_param_config('continuous', nu, nD, lag, sliding_window_size):
                            start = timer()
                            sim_pca.simulate_continuous(nu=nu, lag=lag, nD=nD)
                            end = timer()
                            elapsed = end - start
                            step_sim_cont = "\tContinuous adaptation out of {num_of_out} artificially generated outliers naivly detected {naive_det} ({naive_ratio} ratio)\n." \
                                            "\t\tindepthly detected {indepth_det} ({indepth_ratio} ratio) (Repeated counts: {repeated_counts})\n. " \
                                            "Overall Nu ratio was {all_out_ratio}. The process took {elapsed}s\n".format(
                                num_of_out=len(naive_art_outlier_indexes),
                                naive_det=len(sim_pca.naive_outlier_ct),
                                naive_ratio=len(sim_pca.naive_outlier_ct)/len(naive_art_outlier_indexes),
                                indepth_det=len(sim_pca.indepth_outlier_ct),
                                indepth_ratio=(len(sim_pca.indepth_outlier_ct)-sum(sim_pca.repeated_counts.values())) / len(naive_art_outlier_indexes),
                                repeated_counts=sum(sim_pca.repeated_counts.values()),
                                all_out_ratio=sim_pca.novelty_ct[-1]/(len(df)-sliding_window_size),
                                elapsed=elapsed
                            )
                            analysis_json[df_name]['simulation'][n_component][nu][nD][lag][sliding_window_size]['continuous'] = {'naive_ratio': len(sim_pca.naive_outlier_ct)/len(naive_art_outlier_indexes),
                                                                                                                    'indepth_ratio': (len(sim_pca.indepth_outlier_ct)-sum(sim_pca.repeated_counts.values()))/len(naive_art_outlier_indexes),
                                                                                                                    'repeated_counts': sum(sim_pca.repeated_counts.values()),
                                                                                                                    'all_outliers_ratio': sim_pca.novelty_ct[-1]/(len(df)-sliding_window_size),
                                                                                                                    'time': elapsed}

                        ###2.2 Fixed Freq adapt
                        step_sim_fixed = []
                        for fixed_freq in fixed_freqs:
                            if is_config_banned(cur_dir, sliding_window_size, fixed_freq):
                                print("PCA skipping ineligable config for {} with win size {} and fixed freq {}".format(
                                    cur_dir, sliding_window_size, fixed_freq))
                            else:
                                start = timer()
                                sim_pca.simulate_fixed_freq(nu=nu, lag=lag, nD=nD, fixed_freq=fixed_freq)
                                end = timer()
                                elapsed = end - start
                                step_sim_fixed.append("\tFixed frequency adaptation (fixed_freq={fixed_freq}) out of {num_of_out} artificially generated outliers naivly detected {naive_det} ({naive_ratio} ratio)\n." \
                                    "\t\tindepthly detected {indepth_det} ({indepth_ratio} ratio) (Repeated counts: {repeated_counts})\n. " \
                                    "Overall Nu ratio was {all_out_ratio}. The process took {elapsed}s\n".format(
                                        fixed_freq=fixed_freq,
                                        num_of_out=len(naive_art_outlier_indexes),
                                        naive_det=len(sim_pca.naive_outlier_ct),
                                        naive_ratio=len(sim_pca.naive_outlier_ct) / len(naive_art_outlier_indexes),
                                        indepth_det=len(sim_pca.indepth_outlier_ct),
                                        indepth_ratio=(len(sim_pca.indepth_outlier_ct)-sum(sim_pca.repeated_counts.values())) / len(naive_art_outlier_indexes),
                                        repeated_counts=sum(sim_pca.repeated_counts.values()),
                                        all_out_ratio=sim_pca.novelty_ct[-1] / (len(df) - sliding_window_size),
                                        elapsed=elapsed
                                    ))

                                if fixed_freq not in analysis_json[df_name]['simulation'][n_component][nu][nD][lag][sliding_window_size]['fixed_freq'].keys():
                                    analysis_json[df_name]['simulation'][n_component][nu][nD][lag][sliding_window_size]['fixed_freq'][fixed_freq] = {}
                                analysis_json[df_name]['simulation'][n_component][nu][nD][lag][sliding_window_size]['fixed_freq'][fixed_freq] = {'naive_ratio': len(sim_pca.naive_outlier_ct)/len(naive_art_outlier_indexes),
                                                                                                                                    'indepth_ratio': (len(sim_pca.indepth_outlier_ct)-sum(sim_pca.repeated_counts.values()))/len(naive_art_outlier_indexes),
                                                                                                                                    'repeated_counts': sum(sim_pca.repeated_counts.values()),
                                                                                                                                    'all_outliers_ratio': sim_pca.novelty_ct[-1]/(len(df)-sliding_window_size),
                                                                                                                                    'time': elapsed}
                        step_sim_fixed = ''.join(step_sim_fixed)

                        ###2.3 Dynamic adapt
                        step_sim_dyn = ""
                        if is_good_param_config('dynamic', nu, nD, lag, sliding_window_size):
                            start = timer()
                            sim_pca.simulate_dynamic(nu=nu, lag=lag, nD=nD)
                            end = timer()
                            elapsed = end - start
                            step_sim_dyn = "\tDynamic adaptation out of {num_of_out} artificially generated outliers naivly detected {naive_det} ({naive_ratio} ratio)\n." \
                                    "\t\tindepthly detected {indepth_det} ({indepth_ratio} ratio) (Repeated counts: {repeated_counts})\n. " \
                                    "Overall Nu ratio was {all_out_ratio}. The process took {elapsed}s\n".format(
                                        num_of_out=len(naive_art_outlier_indexes),
                                        naive_det=len(sim_pca.naive_outlier_ct),
                                        naive_ratio=len(sim_pca.naive_outlier_ct) / len(naive_art_outlier_indexes),
                                        indepth_det=len(sim_pca.indepth_outlier_ct),
                                        indepth_ratio=(len(sim_pca.indepth_outlier_ct)-sum(sim_pca.repeated_counts.values()))/ len(naive_art_outlier_indexes),
                                        repeated_counts=sum(sim_pca.repeated_counts.values()),
                                        all_out_ratio=sim_pca.novelty_ct[-1] / (len(df) - sliding_window_size),
                                        elapsed=elapsed
                                    )
                            analysis_json[df_name]['simulation'][n_component][nu][nD][lag][sliding_window_size]['dynamic'] = {'naive_ratio': len(sim_pca.naive_outlier_ct)/len(naive_art_outlier_indexes),
                                                                                                                 'indepth_ratio': (len(sim_pca.indepth_outlier_ct)-sum(sim_pca.repeated_counts.values()))/len(naive_art_outlier_indexes),
                                                                                                                 'repeated_counts': sum(sim_pca.repeated_counts.values()),
                                                                                                                 'all_outliers_ratio': sim_pca.novelty_ct[-1]/(len(df)-sliding_window_size),
                                                                                                                 'time': elapsed}

                        with open("{}/{}.txt".format(path, df_name), "a") as f:
                            f.write(step_base + step_sim + step_sim_cont + step_sim_fixed + step_sim_dyn)
                        step_base = ""

    with open("{}/analysis.json".format(path), "w") as f:
        f.write(json.dumps(analysis_json))