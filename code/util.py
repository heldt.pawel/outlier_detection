import numpy as np
from matplotlib.lines import Line2D

def get_values(df) -> object:
    if len(df.values.shape) != 1:
        return df.values[:, 1]
    return df.values

def create_dim(values, k, nD):  # return vector with t[i], t[i-k] columns
    if nD == 1:
        return values[::-1][::k][::-1].reshape(-1, 1)
    elif nD == 2:
        return np.vstack((values[::-1][:-k:k][::-1], values[::-1][k::k][::-1])).T
    elif nD == 3:
        return np.vstack((values[::-1][:-2*k:k][::-1], values[::-1][k:-k:k][::-1], values[::-1][2*k::k][::-1])).T
    elif nD == 4:
        return np.vstack((values[::-1][:-3 * k:k][::-1], values[::-1][k:-2*k:k][::-1], values[::-1][2 * k:-k:k][::-1], values[::-1][3 * k::k][::-1])).T

def get_visualisation_param(dir):
    if dir == "realTweets":
        colours = {'Baseline': 'k', 'continuous': 'b', 'fixed_freq': 'g', 'dynamic': 'r'}
        opacity = {1: 1, 2: 0.7, 3: 0.5, 4: 0.2}
        size = {72: 10, 288: 50, 2016: 200}  # sliding window size
        marker_type = {12: 'v', 36: '^', 72: '<', 144: '>', 288: 'x', 864: 'd'}
    elif dir == "realKnownCause":
        colours = {'Baseline': 'k', 'continuous': 'b', 'fixed_freq': 'g', 'dynamic': 'r'}
        opacity = {1: 1, 2: 0.7, 3: 0.5, 4: 0.2}
        size = {48: 10, 336: 50, 672: 200}  # sliding window size
        marker_type = {24: 'v', 48: '^', 336: '<', 672: '>'}
    return colours, opacity, size, marker_type

def is_config_banned(cur_dir, config_win_size, config_fixed_freq):
    banned_fixed_freqs_comb = {72: [144, 288, 864], 288: [12, 36, 864],
                               2016: [12, 36, 72]} if cur_dir == "realTweets" else {48: [336, 672], 336: [672],
                                                                                    672: [24]}
    for win_size, banned_fixed_freq_sizes in banned_fixed_freqs_comb.items():
        for banned_fixed_freq_size in banned_fixed_freq_sizes:
            if banned_fixed_freq_size == config_fixed_freq and win_size == config_win_size:
                return True

def get_legend_for_vis(dir):
    if dir == "realKnownCause":
        leg_vis = [Line2D([0], [0], color='w', lw=4),
                   Line2D([0], [0], color='k', lw=4),
                   Line2D([0], [0], color='b', lw=4),
                   Line2D([0], [0], color='g', lw=4),
                   Line2D([0], [0], color='r', lw=4),

                   Line2D([0], [0], color='w', lw=4),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', alpha=1),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', alpha=0.7),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', alpha=0.5),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', alpha=0.2),

                   Line2D([0], [0], color='w', lw=4),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', markersize=4),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', markersize=8),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', markersize=14),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', markersize=22),

                   Line2D([0], [0], color='w', lw=4),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='v', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='^', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='<', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='>', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),

                   ]
        leg_desc = ['Simulation Type',
                    'Baseline',
                    'Continuous',
                    'Fixed Frequency',
                    'Dynamic',

                    'Dimensions',
                    '1D',
                    '2D',
                    '3D',
                    '4D',

                    'Sliding Window Size',
                    'No Sliding Window',
                    'One Day',
                    'One Week',
                    'Two weeks',

                    'Fixed Freqency Value',
                    'No Fixed Frequency',
                    'Half a Day',
                    'One Day',
                    'One Week',
                    'Two Weeks']

    elif dir == "realTweets":
        leg_vis = [Line2D([0], [0], color='w', lw=4),
                   Line2D([0], [0], color='k', lw=4),
                   Line2D([0], [0], color='b', lw=4),
                   Line2D([0], [0], color='g', lw=4),
                   Line2D([0], [0], color='r', lw=4),

                   Line2D([0], [0], color='w', lw=4),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', alpha=1),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', alpha=0.7),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', alpha=0.5),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', alpha=0.2),

                   Line2D([0], [0], color='w', lw=4),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', markersize=4),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', markersize=8),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', markersize=14),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter', markersize=22),

                   Line2D([0], [0], color='w', lw=4),
                   Line2D([0], [0], marker='h', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='v', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='^', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='<', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='>', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='x', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   Line2D([0], [0], marker='d', color='w', markerfacecolor='w', markeredgewidth=1.5,
                          markeredgecolor='k', label='Scatter'),
                   ]
        leg_desc = ['Simulation Type',
                    'Baseline',
                    'Continuous',
                    'Fixed Frequency',
                    'Dynamic',

                    'Dimensions',
                    '1D',
                    '2D',
                    '3D',
                    '4D',

                    'Sliding Window Size',
                    'No Sliding Window',
                    '6 Hours',
                    'One Day',
                    'One Weeks',

                    'Fixed Freqency Value',
                    'No Fixed Frequency',
                    '1 Hour',
                    '3 Hours',
                    '6 Hours',
                    '12 Hours',
                    'One Day',
                    '3 Days']

    return leg_vis, leg_desc
