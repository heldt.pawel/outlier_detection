import os
import matplotlib.pyplot as plt
from graph_creator import GraphCreator
from one_class_svm import OneClassSVM
import numpy as np
from skimage import measure
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.patches as mpatches
import matplotlib.font_manager


class GraphCreatorOneClassSVM(GraphCreator):
    def __init__(self):
        super()

    def create_graph(self, root_dir, df_name, df):
        r, c = 6, 2
        self.fig.set_figheight(48)
        self.fig.set_figwidth(16)
        self.fig.suptitle("One-Class SVM classification for {}/{}".format(root_dir, df_name))

        nDs = (2, 3)
        lags = (1, 2, 3, 10, 20)
        subplotLoc = 3
        for lag in lags:
            for nD in nDs:
                ocsvm = OneClassSVM(df, lag=lag, nD=nD)
                ocsvm.model_fit_predict()
                if nD == 2:
                    ax = self.fig.add_subplot(r, c, subplotLoc)
                    self.create_2d_graph(ax, df_name, ocsvm, nD=nD, lag=lag)
                elif nD == 3:
                    ax = self.fig.add_subplot(r, c, subplotLoc, projection='3d')
                    self.create_3d_graph(ax, df_name, ocsvm, nD=nD, lag=lag)
                subplotLoc += 1

    def create_2d_graph(self, ax, df_name, ocsvm, nD, lag):
        xx, yy = np.meshgrid(np.linspace(int(np.min(ocsvm.values)), int(np.max(ocsvm.values)), 500),
                             np.linspace(int(np.min(ocsvm.values)), int(np.max(ocsvm.values)), 500)) #TODO replace 500 with sth more customizable

        n_error_train = ocsvm.y_pred_train[ocsvm.y_pred_train == -1].size
        n_error_test = ocsvm.y_pred_test[ocsvm.y_pred_test == -1].size

        Z = ocsvm.clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        try:
            ax.contourf(xx, yy, Z, levels=np.linspace(Z.min(), 0, 7), cmap=plt.cm.PuBu)
            a = ax.contour(xx, yy, Z, levels=[0], linewidths=2, colors='darkred')
            ax.contourf(xx, yy, Z, levels=[min(0, Z.max()), max(0, Z.max())], colors='palevioletred')
        except:
            a = None
            pass

        s = 40
        b1 = ax.scatter(ocsvm.val_train[:, 0], ocsvm.val_train[:, 1], c='white', s=s, edgecolors='k')
        b2 = ax.scatter(ocsvm.val_test[:, 0], ocsvm.val_test[:, 1], c='blueviolet', s=s,
                         edgecolors='k')
        ax.axis('tight')
        ax.set_xlim((np.min(ocsvm.values), np.max(ocsvm.values)))
        ax.set_ylim((np.min(ocsvm.values), np.max(ocsvm.values)))
        if a:
            ax.legend([a.collections[0], b1, b2],
                   ["learned frontier", "training observations",
                    "new regular observations"],
                   loc="upper left")
        else:
            ax.legend([b1, b2],
                      ["training observations",
                       "new regular observations"],
                      loc="upper left")
        ax.set_xlabel("t[i]")
        ax.set_ylabel("t[i-{}]".format(lag))
        ax.set_title(
            "%dd, K=%d\nerror train: %d/%d ; errors novel regular: %d/%d ; "
            % (nD, lag, n_error_train, len(ocsvm.y_pred_train), n_error_test, len(ocsvm.y_pred_test)))

    def create_3d_graph(self, ax, df_name, ocsvm, nD, lag):
        xx, yy, zz = np.meshgrid(np.linspace(int(np.min(ocsvm.values)), int(np.max(ocsvm.values)), 100),
                             np.linspace(int(np.min(ocsvm.values)), int(np.max(ocsvm.values)), 100),
                             np.linspace(int(np.min(ocsvm.values)), int(np.max(ocsvm.values)), 100)) #TODO replace 500 with sth more customizable

        n_error_train = ocsvm.y_pred_train[ocsvm.y_pred_train == -1].size
        n_error_test = ocsvm.y_pred_test[ocsvm.y_pred_test == -1].size

        Z = ocsvm.clf.decision_function(np.c_[xx.ravel(), yy.ravel(), zz.ravel()])
        Z = Z.reshape(xx.shape)

        try:
            verts, faces, _, _ = measure.marching_cubes(Z, 0)
            verts = verts * \
                    [np.max(ocsvm.values) - np.min(ocsvm.values), np.max(ocsvm.values) - np.min(ocsvm.values),
                     np.max(ocsvm.values) - np.min(ocsvm.values)] / 100  # space sampling points
            verts = verts + [np.min(ocsvm.values), np.min(ocsvm.values), np.min(ocsvm.values)]

            mesh = Poly3DCollection(verts[faces],
                                    facecolor='darkred', edgecolor='gray', alpha=0.3)
            ax.add_collection3d(mesh)
        except:
            pass

        s = 40
        b1 = ax.scatter(ocsvm.val_train[:, 0], ocsvm.val_train[:, 1], ocsvm.val_train[:, 2], c='white', s=s,
                        edgecolors='k')
        b2 = ax.scatter(ocsvm.val_test[:, 0], ocsvm.val_test[:, 1], ocsvm.val_test[:, 2], c='blueviolet', s=s,
                        edgecolors='k')

        ax.axis('tight')
        ax.set_xlim((np.min(ocsvm.values), np.max(ocsvm.values)))
        ax.set_ylim((np.min(ocsvm.values), np.max(ocsvm.values)))
        ax.set_zlim((np.min(ocsvm.values), np.max(ocsvm.values)))
        ax.set_xlabel("t[i]")
        ax.set_ylabel("t[i-{}]".format(lag))
        ax.set_zlabel("t[i-{}]".format(2*lag))
        ax.legend([mpatches.Patch(color='darkred', alpha=0.3), b1, b2],
                  ["learned frontier", "training observations",
                   "new regular observations"],
                  loc="lower left",
                  prop=matplotlib.font_manager.FontProperties(size=11))
        ax.set_title("%dd, K=%d\nerror train: %d/%d ; errors novel regular: %d/%d ; "
            % (nD, lag, n_error_train, len(ocsvm.y_pred_train), n_error_test, len(ocsvm.y_pred_test)))

    def save_graph(self, root_path, cur_dir, graph_name):
        path = "{}/one_class_svm/{}".format(root_path, cur_dir)
        if not os.path.isdir(path):
            os.makedirs(path)
        self.fig.savefig(os.path.join(path, "{}.png".format(graph_name)))