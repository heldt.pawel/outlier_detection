import matplotlib.pyplot as plt


class GraphCreator(object):
    def __init__(self):
        self.fig = plt.figure()

    def plot_graph(self):
        plt.show()

    def close_figure(self):
        self.fig.clf()
        plt.close()
