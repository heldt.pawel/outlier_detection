import matplotlib
import matplotlib.pyplot as plt
from util import *
import os
import imageio
from pygifsicle import optimize
from one_class_svm import OneClassSVM
import numpy as np
from skimage import measure
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.patches as mpatches
import matplotlib.font_manager
import shutil
from datetime import datetime

class SimulatorSVM(object):
    def __init__(self, root_path, cur_dir, df_name, df, sliding_window_size=1000, naive_outlier_indexes=[], indepth_outlier_indexes=[], visualisation=False):
        self.root_path = root_path
        self.cur_dir = cur_dir
        self.sliding_window_size = sliding_window_size
        self.df_name = df_name
        self.df = df
        self.novelty_ct = [0]
        self.inlier_ct = [0]
        self.redundant_retrain = 0
        self.naive_outlier_indexes = naive_outlier_indexes
        self.naive_outlier_ct = []
        self.indepth_outlier_indexes = indepth_outlier_indexes
        self.indepth_outlier_ct = []
        self.repeated_counts = {}
        self.visualisation = visualisation

        ###only for dynamic adaptation
        self.dynamic_actual_outlier = 0

    def simulate_continuous(self, lag, nD, nu=0.1):
        self._reset_obj_params()
        # set-up
        train_outlier_inlier_ratios = []
        ts_pos = self.sliding_window_size

        # routine start
        sliding_window_df = self.df[1:ts_pos + 1]
        ocsvm = OneClassSVM(nu=nu, lag=lag, nD=nD)
        pred_train = ocsvm.model_fit_predict_only_custom_data(sliding_window_df)
        train_outlier_inlier_ratios.append(pred_train[pred_train == -1].size / len(pred_train))
        while ts_pos < len(self.df) - 1:
            self._log_ts_pos(ts_pos)
            self._visualise_classification(nD, ts_pos, sliding_window_df, lag, ocsvm)

            ts_pos += 1
            sliding_window_df = self.df[ts_pos + 1 - self.sliding_window_size:ts_pos+1]
            new_elem = ocsvm.model_latest_datapoint_prediction(sliding_window_df)  # check if next elem is an inlier
            self._check_if_outlier(ts_pos, new_elem, nD, lag)

            # retrain model on new sliding window
            pred_train = ocsvm.model_fit_predict_only_custom_data(sliding_window_df)
            train_outlier_inlier_ratios.append(pred_train[pred_train == -1].size / len(pred_train))

            # did the classification of prediction change?
            if pred_train[-1] == new_elem:
                self.redundant_retrain += 1

        # plot outlier/inlier ratios over time
        if self.visualisation:
            # self.plot_information_about_outliers_and_inliers(train_outlier_inlier_ratios, lag, nD)
            self.create_gif_of_clf_process(nD, lag, sim_type="continuous")
            # self.create_log(lag, nD, sim_type="continuous")

    def simulate_fixed_freq(self, fixed_freq, lag, nD, nu=0.1):
        self._reset_obj_params()
        # set-up
        train_outlier_inlier_ratios = []
        self.redundant_retrain = []
        stash_of_points = []
        ts_pos = self.sliding_window_size

        # routine start
        ocsvm = OneClassSVM(nu=nu, lag=lag, nD=nD)
        while ts_pos < len(self.df) - 1:
            self._log_ts_pos(ts_pos)

            if ts_pos % fixed_freq == 0 or ts_pos == self.sliding_window_size:
                sliding_window_df = self.df[ts_pos - self.sliding_window_size +1:ts_pos+1]
                pred_train = ocsvm.model_fit_predict_only_custom_data(sliding_window_df)
                train_outlier_inlier_ratios.append(pred_train[pred_train == -1].size / len(pred_train))
                self._check_if_outlier(ts_pos, pred_train[-1], nD, lag)
                self._visualise_classification(nD, ts_pos, sliding_window_df, lag, ocsvm)
                if stash_of_points:
                    stash_of_points = [a + b for a, b in zip(stash_of_points, pred_train[-len(stash_of_points)-1:-1])] #from pred train take datapoint respective to stash of points( exclude the most recent datapoint in pred train since it is not present in stash_of_points)
                    try:
                        self.redundant_retrain.append((len(stash_of_points) - stash_of_points.count(0))/len(stash_of_points)) #the higher the ratio the more useless the retrain was
                    except:
                        self.redundant_retrain.append(1)
            else:
                actual_sliding_window = self.df[ts_pos - self.sliding_window_size+1:ts_pos+1]
                new_elem = ocsvm.model_latest_datapoint_prediction(actual_sliding_window)  # check if next elem is an inlier
                self._check_if_outlier(ts_pos, new_elem, nD, lag)
                stash_of_points.append(new_elem)

            ts_pos += 1

        # plot outlier/inlier ratios over time
        if self.visualisation:
            # self.plot_information_about_outliers_and_inliers(train_outlier_inlier_ratios, lag, nD, sim_type="fixed_freq", fixed_freq=fixed_freq)
            self.create_gif_of_clf_process(nD, lag, sim_type="fixed_freq")
            # self.create_log(lag, nD, sim_type="fixed_freq")

    def simulate_dynamic(self, lag, nD, nu=0.1):
        self._reset_obj_params()
        # set-up
        train_outlier_inlier_ratios = []
        ts_pos = self.sliding_window_size

        # routine start
        sliding_window_df = self.df[1:ts_pos+1]
        ocsvm = OneClassSVM(nu=nu, lag=lag, nD=nD)
        pred_train = ocsvm.model_fit_predict_only_custom_data(sliding_window_df)
        self._visualise_classification(nD, ts_pos, sliding_window_df, lag, ocsvm)
        while ts_pos < len(self.df) - 1:
            self._log_ts_pos(ts_pos)

            train_outlier_inlier_ratios.append(pred_train[pred_train == -1].size / len(pred_train))
            ts_pos += 1
            sliding_window_df = self.df[ts_pos - self.sliding_window_size+1:ts_pos+1]
            new_elem = ocsvm.model_latest_datapoint_prediction(sliding_window_df)  # check if next elem is an inlier
            self._check_if_outlier(ts_pos, new_elem, nD, lag)
            if new_elem == -1:
                pred_train = ocsvm.model_fit_predict_only_custom_data(sliding_window_df)
                self._visualise_classification(nD, ts_pos, sliding_window_df, lag, ocsvm)
                if pred_train[-1] == new_elem:
                    self.dynamic_actual_outlier += 1
        train_outlier_inlier_ratios.append(pred_train[pred_train == -1].size / len(pred_train)) #calculate ratio one last time

        # plot outlier/inlier ratios over time
        if self.visualisation:
            # self.plot_information_about_outliers_and_inliers(train_outlier_inlier_ratios, lag, nD, sim_type="dynamic")
            self.create_gif_of_clf_process(nD, lag, sim_type="dynamic")
            # self.create_log(lag, nD, sim_type="dynamic", true_out=self.dynamic_actual_outlier)


    def plot_information_about_outliers_and_inliers(self, train_outlier_inlier_ratios, lag, nD, sim_type="continuous", fixed_freq=1):
        fig = plt.figure()
        fig.suptitle("{} adaptation for OCSVM".format(sim_type))
        fig.set_figheight(35)
        fig.set_figwidth(40)
        ax = fig.add_subplot(4, 1, 2, title="Ratio of outliers through {} OCSVM classification".format(sim_type))
        ax.scatter(list(range(self.sliding_window_size, len(self.df), fixed_freq)), train_outlier_inlier_ratios)
        ax.set_xlabel("x[i]")
        ax.set_ylabel("outliers/inliers+outliers")

        ax = fig.add_subplot(4, 1, 3, title="Increase in number of outliers and inliers")
        ax.set_xlabel("x[i]")
        ax.set_ylabel("Amounts of points")
        ax.plot(list(range(self.sliding_window_size, len(self.df))), self.novelty_ct, 'r', label="outliers")
        ax.plot(list(range(self.sliding_window_size, len(self.df))), self.inlier_ct, 'b', label="inliers")
        ax.legend(loc="upper left")

        if sim_type == "fixed_freq":
            ax = fig.add_subplot(4, 1, 4, title="Ratio of how redundant the retrain was (how many novel point got reclassified after retrain), the higher the ratio the more redundant it was")
            ax.set_xlabel("x[i]")
            ax.set_ylabel("ratio [(Length of window - reclassified points)/length of sliding window]")
            ax.scatter(list(range(self.sliding_window_size+fixed_freq, len(self.df), fixed_freq)), self.redundant_retrain)

        path = "{}/simulation/one_class_svm/{}/{}".format(self.root_path, self.cur_dir, self.df_name)
        if not os.path.isdir(path):
            os.makedirs(path)
        plt.savefig(os.path.join(path, "{}D_K_{}_{}_outlier_inlier_clf_overview.png".format(nD, lag, sim_type)))

    def visualise_ocsvm_classification_2d(self, ts_pos, sliding_window_df, lag, nD, ocsvm):
        val = create_dim(get_values(sliding_window_df), lag, nD)
        xx, yy = np.meshgrid(np.linspace(int(np.min(val)), int(np.max(val)), 100),
                             np.linspace(int(np.min(val)), int(np.max(val)), 100))
        Z = ocsvm.clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        plt.title("{}/{} Novelty Detection, step: {}/{}".format(self.cur_dir[:8], self.df_name, ts_pos, len(self.df)))
        try:
            plt.contourf(xx, yy, Z, levels=np.linspace(Z.min(), 0, 7), cmap=plt.cm.PuBu)
            a = plt.contour(xx, yy, Z, levels=[0], linewidths=2, colors='darkred')
            plt.contourf(xx, yy, Z, levels=[min(0, Z.max()), max(0, Z.max())], colors='palevioletred')
        except:
            a = None
        s = 40
        b1 = plt.scatter(val[:, 0], val[:, 1], c='white', s=s, edgecolors='k')
        plt.axis('tight')
        plt.xlim((np.min(val), np.max(val)))
        plt.ylim((np.min(val), np.max(val)))

        if a:
            plt.legend([a.collections[0], b1],
                       ["learned frontier", "training observations"], loc="upper left")
        else:
            plt.legend([b1],
                       ["training observations"], loc="upper left")
        plt.xlabel("t[i]")
        plt.ylabel("t[i-{}]".format(lag))

        path = "{}/simulation/one_class_svm/{}/{}/temp".format(self.root_path, self.cur_dir, self.df_name)
        if not os.path.isdir(path):
            os.makedirs(path)
        plt.savefig(os.path.join(path, "{:07d}.png".format(ts_pos)), dpi=40)
        plt.close()

    def visualise_ocsvm_classification_3d(self, ts_pos, sliding_window_df, lag, nD, ocsvm):
        fig = plt.figure()
        fig.suptitle("{}/{} Novelty Detection, step: {}/{}".format(self.cur_dir[:8], self.df_name, ts_pos, len(self.df)))
        ax = fig.add_subplot(1, 1, 1, projection="3d")
        val = create_dim(get_values(sliding_window_df), lag, nD)
        xx, yy, zz = np.meshgrid(np.linspace(int(np.min(val)), int(np.max(val)), 50),
                                 np.linspace(int(np.min(val)), int(np.max(val)), 50),
                                 np.linspace(int(np.min(val)), int(np.max(val)), 50))
        Z = ocsvm.clf.decision_function(np.c_[xx.ravel(), yy.ravel(), zz.ravel()])
        Z = Z.reshape(xx.shape)

        try:
            verts, faces, _, _ = measure.marching_cubes(Z, 0)
            verts = verts * \
                    [np.max(val) - np.min(val), np.max(val) - np.min(val),
                     np.max(val) - np.min(val)] / 100  # space sampling points
            verts = verts + [np.min(val), np.min(val), np.min(val)]

            mesh = Poly3DCollection(verts[faces], facecolor='darkred', edgecolor='gray', alpha=0.3)
            ax.add_collection3d(mesh)
        except:
            pass
        s = 40
        b1 = ax.scatter(val[:, 0], val[:, 1], val[:, 2], c='white', s=s,
                        edgecolors='k')
        ax.axis('tight')
        ax.set_xlim((np.min(val), np.max(val)))
        ax.set_ylim((np.min(val), np.max(val)))
        ax.set_zlim((np.min(val), np.max(val)))
        ax.set_xlabel("t[i]")
        ax.set_ylabel("t[i-{}]".format(lag))
        ax.set_zlabel("t[i-{}]".format(2 * lag))
        ax.legend([mpatches.Patch(color='darkred', alpha=0.3), b1],
                  ["learned frontier", "training observations"],
                  loc="lower left",
                  prop=matplotlib.font_manager.FontProperties(size=11))

        path = "{}/simulation/one_class_svm/{}/{}/temp".format(self.root_path, self.cur_dir, self.df_name)
        if not os.path.isdir(path):
            os.makedirs(path)
        fig.savefig(os.path.join(path, "{:07d}.png".format(ts_pos)), dpi=40)
        fig.clf()
        plt.close()

    def create_gif_of_clf_process(self, nD, lag, sim_type):
        path_to_tmp_images = "{}/simulation/one_class_svm/{}/{}/temp".format(self.root_path, self.cur_dir, self.df_name)
        path_to_gif = "{}/simulation/one_class_svm/{}/{}/{}D_K_{}_{}_adapt_vis.gif".format(self.root_path, self.cur_dir,
                                                                                          self.df_name, nD, lag, sim_type)
        images = os.listdir(path_to_tmp_images)
        images.sort()

        fps = 45 if sim_type == "continuous" else 10
        with imageio.get_writer(path_to_gif, mode='I', fps=fps) as writer:
            for filename in images:
                image = imageio.imread(os.path.join(path_to_tmp_images, filename))
                writer.append_data(image)
        optimize(path_to_gif)
        shutil.rmtree(path_to_tmp_images)

    def create_log(self, lag, nD, sim_type, true_out= "N/A", fixed_freq="N/A"):
        path = "{}/simulation/one_class_svm/{}/{}/run_overview.txt".format(self.root_path, self.cur_dir, self.df_name)
        with open(path, 'a') as f:
            if sim_type == "continuous":
                red_retrain = self.redundant_retrain
            elif sim_type == "fixed_freq":
                red_retrain = "Retrains were nothing changed:{}, check graph for insights".format(self.redundant_retrain.count(1))
            elif sim_type == "dynamic":
                red_retrain = "N/a"
            overview = "\n\n{sim_type}, {nD}D, K={lag} {dir}/{name}" \
                       "\n\tInliers detected {inlier_ct}" \
                       "\n\tOutliers detected {outlier_ct}" \
                       "\n\tRedundant retrains {red_ret_ct}" \
                       "\n\tSliding window size {sliding_window_size}" \
                       "\n\tAmount of datapoints {data_len}" \
                       "\n\tTrue outliers detected (dynamic simulation only) {true_out}" \
                       "\n\tFixed frequency size {fixed_freq}".format(
                                                                    sim_type=sim_type,
                                                                    nD=nD,
                                                                    lag=lag,
                                                                    dir=self.cur_dir,
                                                                    name=self.df_name,
                                                                    inlier_ct=self.inlier_ct[-1],
                                                                    outlier_ct=self.novelty_ct[-1],
                                                                    red_ret_ct=red_retrain,
                                                                    sliding_window_size=self.sliding_window_size,
                                                                    data_len=len(self.df),
                                                                    true_out=true_out,
                                                                    fixed_freq=fixed_freq,
                                                                    )
            f.write(overview)

    def _check_if_outlier(self, ts_pos, elem, nD, lag):
        if elem == -1:
            self.novelty_ct.append(self.novelty_ct[-1] + 1)
            self.inlier_ct.append(self.inlier_ct[-1])
            if ts_pos in self.naive_outlier_indexes:
                self.naive_outlier_ct.append(ts_pos)
            if ts_pos in self.indepth_outlier_indexes:
                self.indepth_outlier_ct.append(ts_pos)

                #find indexes of artificial outliers within indexes
                potential_indexes = set(ts_pos - (d*lag) for d in range(nD)) & set(self.naive_outlier_indexes)
                for index in potential_indexes:
                    if index in self.repeated_counts.keys():
                        self.repeated_counts[index] += 1
                    else:
                        self.repeated_counts[index] = 0 #starting with zero to avoid substracting first example later on

        elif elem == 1:
            self.novelty_ct.append(self.novelty_ct[-1])
            self.inlier_ct.append(self.inlier_ct[-1] + 1)

    def _visualise_classification(self, nD, ts_pos, sliding_window_df, lag, ocsvm):
        if self.visualisation:
            if nD == 2:
                self.visualise_ocsvm_classification_2d(ts_pos, sliding_window_df, lag, nD, ocsvm)
            elif nD == 3:
                self.visualise_ocsvm_classification_3d(ts_pos, sliding_window_df, lag, nD, ocsvm)

    def _reset_obj_params(self):
        self.novelty_ct = [0]
        self.inlier_ct = [0]
        self.redundant_retrain = 0
        self.naive_outlier_ct = []
        self.indepth_outlier_ct = []
        self.repeated_counts = {}

    def _log_ts_pos(self, ts_pos):
        if ts_pos % 100 == 0:
            print("{}    {}/{} - {}/{}".format(datetime.now().strftime("%H:%M:%S"), self.cur_dir, self.df_name, ts_pos,
                                               len(self.df)))
