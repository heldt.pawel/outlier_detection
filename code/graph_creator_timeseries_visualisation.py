import os
import matplotlib.pyplot as plt
from util import get_values
from graph_creator import GraphCreator

class GraphCreatorTimeSeriesVisualisation(GraphCreator):
    def __init__(self):
        super().__init__()

    def create_graph(self, root_dir, df_name, df):
        SMALL_SIZE = 24
        MEDIUM_SIZE = 34
        BIGGER_SIZE = 36

        plt.rc('font', size=BIGGER_SIZE)  # controls default text sizes
        plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
        plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
        plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
        plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
        plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

        r, c = 1, 1
        if root_dir == "realTweets":
            df = df[8092:10396]
            title = "AAPL tweets time-series visualisation between 27/3 - 4/4 2017\n"
        elif root_dir == "realKnownCause":
            df = df[:576]
            title = "NYC Taxi's rides time-series visualisation between\n 1/7 - 12/7 2015\n"
        values = get_values(df)
        self.fig.set_figheight(16)
        self.fig.set_figwidth(16)
        # self.fig.suptitle("{}/{}".format(root_dir, df_name))

        ax1 = self.fig.add_subplot(r, c, 1, title=title)
        ax1.set_xlabel("Time")
        ax1.set_ylabel("Amount")

        ax1.plot(df.timestamp, values, linewidth=6)
        ax1.set_xticks(df.timestamp[0::80])
        ax1.set_xticklabels(df.timestamp[0::80], rotation=35)
        plt.show()
        print(9)

    def save_graph(self, root_path, cur_dir, graph_name):
        path = "{}/graphs/{}".format(root_path, cur_dir)
        if not os.path.isdir(path):
            os.makedirs(path)
        self.fig.savefig(os.path.join(path, "ts_{}.png".format(graph_name)))
