from one_class_svm import OneClassSVM
from simulator_one_class_svm import SimulatorSVM
import os
from df_loader import DfLoader
from collections import Counter
import json
import pandas as pd
import matplotlib.pyplot as plt
from const.wrong_param_configs import is_good_param_config
from timeit import default_timer as timer
import numpy as np
from util import get_visualisation_param, get_legend_for_vis, is_config_banned


def compare_simulation_with_baseline(df, root_path, cur_dir, df_name, outlier_indexes=[], nu_vals=[0.1], lags=[1], dims=[3], sliding_window_sizes=[1000], fixed_freqs=[50]):
    path = "{}/analysis_OCSVM/ocsvm_simulation_comparison/{}".format(root_path, cur_dir)
    if not os.path.isdir(path):
        os.makedirs(path)

    if os.path.isfile("{}/analysis.json".format(path)):
        with open("{}/analysis.json".format(path), 'r') as f:
            analysis_json = json.loads(f.read())
    else:
        analysis_json = {}

    if df_name not in analysis_json.keys():
        analysis_json[df_name] = {}

    ### Step 1: Run Baseline
    for nu in nu_vals:
        for nD in dims:
            for lag in lags:
                base_ocsvm = OneClassSVM(nu=nu, lag=lag, nD=nD)
                start = timer()
                pred = base_ocsvm.model_fit_predict_only_custom_data(df)
                end = timer()
                elapsed = end-start

                #classification of artificially generated outliers
                pred_indexes = {k: v for k, v in zip(list(range(len(df)-1, len(df)-1 - (len(pred)-1)*lag -1, -lag))[::-1], pred)}

                #outlier detection treating the whole vector as a whole
                art_outlier_pred = [pred_indexes.get(index) for index in outlier_indexes if index in pred_indexes.keys()]

                #outlier detection looking if oulier was spotted in any vector containg that outlier, no matter on which position it is
                indepth_outlier_pred = {}
                for index in outlier_indexes: #collecting every data-points tainted with artificially generated outliers
                    for d in range(0, nD):
                        if pred_indexes.get(index + (d * lag)) == -1:
                            if indepth_outlier_pred.get(index, None):
                                indepth_outlier_pred[index] += 1
                            else:
                                indepth_outlier_pred[index] = 1

                naive_artificial_outlier_num = Counter(art_outlier_pred)[-1]
                indepth_artificial_outlier_num = 0
                repeated_counts = 0
                for k, v in indepth_outlier_pred.items():
                    indepth_artificial_outlier_num += 1
                    if v != 1:
                        repeated_counts += (v-1)

                step_base = "\n\nBaseline Run of OCSVM nu={nu}, {nD}D, K={lag} with classification of the whole TS {cur_dir}/{df_name}\n" \
                                  "\tClassification of {data_len} datapoints ({data_len_including_lag} data-points including lag), {num_of_found_outliers} were classified as outliers, giving the nu={nu_ratio}\n" \
                                  "\tOut of {num_of_gen_outliers} artificially generated outliers, {naive_gen_outliers_found} were naivly detected ({naive_ratio} Ratio)\n" \
                                  "\tOut of {num_of_gen_outliers} artificially generated outliers, {indepth_gen_outliers_found} were indepthly detected ({indepth_ratio} Ratio) (Repeated counts amounted to {repeated_counts})\n" \
                                  "\tThe process took {elapsed}s\n".format(
                    nu=nu,
                    nD=nD,
                    lag=lag,
                    cur_dir=cur_dir,
                    df_name=df_name,
                    data_len=len(df),
                    data_len_including_lag=len(pred),
                    num_of_found_outliers=Counter(pred)[-1],
                    nu_ratio=Counter(pred)[-1] / len(pred),
                    num_of_gen_outliers=len(outlier_indexes),
                    naive_gen_outliers_found=naive_artificial_outlier_num,
                    naive_ratio=naive_artificial_outlier_num / len(art_outlier_pred),
                    indepth_gen_outliers_found=indepth_artificial_outlier_num,
                    indepth_ratio=(indepth_artificial_outlier_num) / len(art_outlier_pred),
                    repeated_counts=repeated_counts,
                    elapsed=elapsed
                )

                if 'baseline' not in analysis_json[df_name].keys():
                    analysis_json[df_name]['baseline'] = {}
                if nu not in analysis_json[df_name]['baseline'].keys():
                    analysis_json[df_name]['baseline'][nu] = {}
                if nD not in analysis_json[df_name]['baseline'][nu].keys():
                    analysis_json[df_name]['baseline'][nu][nD] = {}
                if lag not in analysis_json[df_name]['baseline'][nu][nD].keys():
                    analysis_json[df_name]['baseline'][nu][nD][lag] = {}

                analysis_json[df_name]['baseline'][nu][nD][lag] = {'naive_ratio': naive_artificial_outlier_num / len(art_outlier_pred),
                                                                   'indepth_ratio': indepth_artificial_outlier_num/len(art_outlier_pred),
                                                                   'repeated_counts': repeated_counts,
                                                                   'all_outliers_ratio': Counter(pred)[-1] / len(pred),
                                                                   'time': elapsed}

                ## Step2: Run for sliding window adapts
                for sliding_window_size in sliding_window_sizes:
                    if lag * nD >= sliding_window_size:
                        continue

                    print("\n\n\n\n\nnu={}, dim={}, lag={}, sliding_window={}".format(nu, nD, lag, sliding_window_size))

                    naive_art_outlier_indexes = [i for i in outlier_indexes if i > sliding_window_size]
                    indepth_art_outlier_indexes = list(set(i + (d * lag) for d in range(nD) for i in outlier_indexes if i > sliding_window_size))
                    sim_ocsvm = SimulatorSVM(root_path, cur_dir, df_name, df, sliding_window_size=sliding_window_size, naive_outlier_indexes=naive_art_outlier_indexes, indepth_outlier_indexes=indepth_art_outlier_indexes, visualisation=False)

                    step_sim = "\nSimulation of sliding window adaptations with nu={nu}, {win_size} window size, K={lag} and {nD} dimensions\n".format(
                        nu=nu,
                        win_size=sliding_window_size,
                        lag=lag,
                        nD=nD
                    )

                    if 'simulation' not in analysis_json[df_name].keys():
                        analysis_json[df_name]['simulation'] = {}
                    if nu not in analysis_json[df_name]['simulation'].keys():
                        analysis_json[df_name]['simulation'][nu] = {}
                    if nD not in analysis_json[df_name]['simulation'][nu].keys():
                        analysis_json[df_name]['simulation'][nu][nD] = {}
                    if lag not in analysis_json[df_name]['simulation'][nu][nD].keys():
                        analysis_json[df_name]['simulation'][nu][nD][lag] = {}
                    if sliding_window_size not in analysis_json[df_name]['simulation'][nu][nD][lag]:
                        analysis_json[df_name]['simulation'][nu][nD][lag][sliding_window_size] = {'continuous': {}, 'fixed_freq': {}, 'dynamic': {}}

                    #2.1 Continuous adapt
                    step_sim_cont = ""
                    if is_good_param_config('continuous', nu, nD, lag, sliding_window_size):
                        start = timer()
                        sim_ocsvm.simulate_continuous(nu=nu, lag=lag, nD=nD)
                        end = timer()
                        elapsed = end - start
                        step_sim_cont = "\tContinuous adaptation out of {num_of_out} artificially generated outliers naivly detected {naive_det} ({naive_ratio} ratio)\n." \
                                        "\t\tindepthly detected {indepth_det} ({indepth_ratio} ratio) (Repeated counts: {repeated_counts})\n. " \
                                        "Overall Nu ratio was {all_out_ratio}. The process took {elapsed}s\n".format(
                            num_of_out=len(naive_art_outlier_indexes),
                            naive_det=len(sim_ocsvm.naive_outlier_ct),
                            naive_ratio=len(sim_ocsvm.naive_outlier_ct)/len(naive_art_outlier_indexes),
                            indepth_det=len(sim_ocsvm.indepth_outlier_ct),
                            indepth_ratio=(len(sim_ocsvm.indepth_outlier_ct)-sum(sim_ocsvm.repeated_counts.values())) / len(naive_art_outlier_indexes),
                            repeated_counts=sum(sim_ocsvm.repeated_counts.values()),
                            all_out_ratio=sim_ocsvm.novelty_ct[-1]/(len(df)-sliding_window_size),
                            elapsed=elapsed
                        )
                        analysis_json[df_name]['simulation'][nu][nD][lag][sliding_window_size]['continuous'] = {'naive_ratio': len(sim_ocsvm.naive_outlier_ct)/len(naive_art_outlier_indexes),
                                                                                                                'indepth_ratio': (len(sim_ocsvm.indepth_outlier_ct)-sum(sim_ocsvm.repeated_counts.values()))/len(naive_art_outlier_indexes),
                                                                                                                'repeated_counts': sum(sim_ocsvm.repeated_counts.values()),
                                                                                                                'all_outliers_ratio': sim_ocsvm.novelty_ct[-1]/(len(df)-sliding_window_size),
                                                                                                                'time': elapsed}

                    ##2.2 Fixed Freq adapt
                    step_sim_fixed = []
                    for fixed_freq in fixed_freqs:
                        if is_config_banned(cur_dir, sliding_window_size, fixed_freq):
                            print("OCSVM skipping ineligable config for {} with win size {} and fixed freq {}".format(cur_dir, sliding_window_size, fixed_freq))
                        else:
                            start = timer()
                            sim_ocsvm.simulate_fixed_freq(nu=nu, lag=lag, nD=nD, fixed_freq=fixed_freq)
                            end = timer()
                            elapsed = end - start
                            step_sim_fixed.append("\tFixed frequency adaptation (fixed_freq={fixed_freq}) out of {num_of_out} artificially generated outliers naivly detected {naive_det} ({naive_ratio} ratio)\n." \
                                "\t\tindepthly detected {indepth_det} ({indepth_ratio} ratio) (Repeated counts: {repeated_counts})\n. " \
                                "Overall Nu ratio was {all_out_ratio}. The process took {elapsed}s\n".format(
                                    fixed_freq=fixed_freq,
                                    num_of_out=len(naive_art_outlier_indexes),
                                    naive_det=len(sim_ocsvm.naive_outlier_ct),
                                    naive_ratio=len(sim_ocsvm.naive_outlier_ct) / len(naive_art_outlier_indexes),
                                    indepth_det=len(sim_ocsvm.indepth_outlier_ct),
                                    indepth_ratio=(len(sim_ocsvm.indepth_outlier_ct)-sum(sim_ocsvm.repeated_counts.values())) / len(naive_art_outlier_indexes),
                                    repeated_counts=sum(sim_ocsvm.repeated_counts.values()),
                                    all_out_ratio=sim_ocsvm.novelty_ct[-1] / (len(df) - sliding_window_size),
                                    elapsed=elapsed
                                ))

                            if fixed_freq not in analysis_json[df_name]['simulation'][nu][nD][lag][sliding_window_size]['fixed_freq'].keys():
                                analysis_json[df_name]['simulation'][nu][nD][lag][sliding_window_size]['fixed_freq'][fixed_freq] = {}
                            analysis_json[df_name]['simulation'][nu][nD][lag][sliding_window_size]['fixed_freq'][fixed_freq] = {'naive_ratio': len(sim_ocsvm.naive_outlier_ct)/len(naive_art_outlier_indexes),
                                                                                                                                'indepth_ratio': (len(sim_ocsvm.indepth_outlier_ct)-sum(sim_ocsvm.repeated_counts.values()))/len(naive_art_outlier_indexes),
                                                                                                                                'repeated_counts': sum(sim_ocsvm.repeated_counts.values()),
                                                                                                                                'all_outliers_ratio': sim_ocsvm.novelty_ct[-1]/(len(df)-sliding_window_size),
                                                                                                                                'time': elapsed}
                    step_sim_fixed = ''.join(step_sim_fixed)

                    ##2.3 Dynamic adapt
                    step_sim_dyn = ""
                    if is_good_param_config('dynamic', nu, nD, lag, sliding_window_size):
                        start = timer()
                        sim_ocsvm.simulate_dynamic(nu=nu, lag=lag, nD=nD)
                        end = timer()
                        elapsed = end - start
                        step_sim_dyn = "\tDynamic adaptation out of {num_of_out} artificially generated outliers naivly detected {naive_det} ({naive_ratio} ratio)\n." \
                                "\t\tindepthly detected {indepth_det} ({indepth_ratio} ratio) (Repeated counts: {repeated_counts})\n. " \
                                "Overall Nu ratio was {all_out_ratio}. The process took {elapsed}s\n".format(
                                    num_of_out=len(naive_art_outlier_indexes),
                                    naive_det=len(sim_ocsvm.naive_outlier_ct),
                                    naive_ratio=len(sim_ocsvm.naive_outlier_ct) / len(naive_art_outlier_indexes),
                                    indepth_det=len(sim_ocsvm.indepth_outlier_ct),
                                    indepth_ratio=(len(sim_ocsvm.indepth_outlier_ct)-sum(sim_ocsvm.repeated_counts.values()))/ len(naive_art_outlier_indexes),
                                    repeated_counts=sum(sim_ocsvm.repeated_counts.values()),
                                    all_out_ratio=sim_ocsvm.novelty_ct[-1] / (len(df) - sliding_window_size),
                                    elapsed=elapsed
                                )
                        analysis_json[df_name]['simulation'][nu][nD][lag][sliding_window_size]['dynamic'] = {'naive_ratio': len(sim_ocsvm.naive_outlier_ct)/len(naive_art_outlier_indexes),
                                                                                                             'indepth_ratio': (len(sim_ocsvm.indepth_outlier_ct)-sum(sim_ocsvm.repeated_counts.values()))/len(naive_art_outlier_indexes),
                                                                                                             'repeated_counts': sum(sim_ocsvm.repeated_counts.values()),
                                                                                                             'all_outliers_ratio': sim_ocsvm.novelty_ct[-1]/(len(df)-sliding_window_size),
                                                                                                             'time': elapsed}

                    with open("{}/{}.txt".format(path, df_name), "a") as f:
                        f.write(step_base + step_sim + step_sim_cont + step_sim_fixed + step_sim_dyn)
                step_base = ""

    with open("{}/analysis.json".format(path), "w") as f:
        f.write(json.dumps(analysis_json))


def report_simulation_with_baseline_comparison(root_path, cur_dir, method="OCSVM"):
    path = "{}/analysis_{}/{}_simulation_comparison/{}".format(root_path, method, method.lower(), cur_dir)

    if not os.path.isfile("{}/analysis.json".format(path)):
        print("Comparison has not been run")
        return
    with open("{}/analysis.json".format(path), 'r') as f:
        analysis_json = json.loads(f.read())

    dataset_reports = []

    columns = ["simulation_type", "nu", "dimension", "lag", "sliding_window_size", "fixed_frequency",
               "naive_artificial_outlier_detection_ratio", "indepth_artificial_outlier_detection_ratio",
               "repeated_counts", "all_outliers_ratio", "time"]
    if method == "PCA":
        columns.append("n_component")

    for dataset_name in analysis_json.keys():
        dataset_report = pd.DataFrame(columns=columns)

        ###Step1 - deal with baseline
        baseline_rows = []
        baseline_vals = analysis_json[dataset_name]['baseline']

        if method == "OCSVM":
            for nu in baseline_vals.keys():
                for nD in baseline_vals[nu].keys():
                    for lag in baseline_vals[nu][nD].keys():
                        naive_art_out_ratio = baseline_vals[nu][nD][lag]['naive_ratio']
                        indepth_ratio = baseline_vals[nu][nD][lag]['indepth_ratio']
                        repeated_counts = baseline_vals[nu][nD][lag]['repeated_counts']
                        all_out_ratio = baseline_vals[nu][nD][lag]['all_outliers_ratio']
                        time = baseline_vals[nu][nD][lag]['time']
                        baseline_rows.append(['Baseline', nu, nD, lag, None, None, naive_art_out_ratio, indepth_ratio, repeated_counts, all_out_ratio, time])
        elif method == "PCA":
            for nu in baseline_vals.keys():
                for n_comp in baseline_vals[nu].keys():
                    for nD in baseline_vals[nu][n_comp].keys():
                        for lag in baseline_vals[nu][n_comp][nD].keys():

                            naive_art_out_ratio = baseline_vals[nu][n_comp][nD][lag]['naive_ratio']
                            indepth_ratio = baseline_vals[nu][n_comp][nD][lag]['indepth_ratio']
                            repeated_counts = baseline_vals[nu][n_comp][nD][lag]['repeated_counts']
                            all_out_ratio = baseline_vals[nu][n_comp][nD][lag]['all_outliers_ratio']
                            time = baseline_vals[nu][n_comp][nD][lag]['time']
                            baseline_rows.append(['Baseline', nu, nD, lag, None, None, naive_art_out_ratio, indepth_ratio, repeated_counts, all_out_ratio, time, n_comp])

        baseline_rows = pd.DataFrame(baseline_rows, columns=columns)
        dataset_report = pd.concat([dataset_report, baseline_rows], ignore_index=True)

        ###Step2 - deal with sliding windows adaptations
        simulation_rows = []
        simulation_vals = analysis_json[dataset_name]['simulation']
        if method == "OCSVM":
            for nu in simulation_vals.keys():
                for nD in simulation_vals[nu].keys():
                    for lag in simulation_vals[nu][nD].keys():
                        for sliding_window_size in simulation_vals[nu][nD][lag].keys():
                            for sim_type in simulation_vals[nu][nD][lag][sliding_window_size].keys():
                                if sim_type == 'fixed_freq':
                                    for freq_val in simulation_vals[nu][nD][lag][sliding_window_size][sim_type].keys():
                                        naive_art_out_ratio = simulation_vals[nu][nD][lag][sliding_window_size][sim_type][freq_val].get('naive_ratio', 0)
                                        indepth_ratio = simulation_vals[nu][nD][lag][sliding_window_size][sim_type][freq_val].get('indepth_ratio', 0)
                                        repeated_counts = simulation_vals[nu][nD][lag][sliding_window_size][sim_type][freq_val].get('repeated_counts', 0)
                                        all_out_ratio = simulation_vals[nu][nD][lag][sliding_window_size][sim_type][freq_val].get('all_outliers_ratio', 0)
                                        time = simulation_vals[nu][nD][lag][sliding_window_size][sim_type][freq_val].get('time', 9999999)
                                        simulation_rows.append([sim_type, nu, nD, lag, sliding_window_size, freq_val, naive_art_out_ratio, indepth_ratio, repeated_counts, all_out_ratio, time])
                                else:
                                    naive_art_out_ratio = simulation_vals[nu][nD][lag][sliding_window_size][sim_type].get('naive_ratio', 0)
                                    indepth_ratio = simulation_vals[nu][nD][lag][sliding_window_size][sim_type].get('indepth_ratio', 0)
                                    repeated_counts = simulation_vals[nu][nD][lag][sliding_window_size][sim_type].get('repeated_counts', 0)
                                    all_out_ratio = simulation_vals[nu][nD][lag][sliding_window_size][sim_type].get('all_outliers_ratio', 0)
                                    time = simulation_vals[nu][nD][lag][sliding_window_size][sim_type].get('time', 99999999)
                                    simulation_rows.append([sim_type, nu, nD, lag, sliding_window_size, None, naive_art_out_ratio, indepth_ratio, repeated_counts, all_out_ratio, time])
        elif method == "PCA":
            for n_comp in simulation_vals.keys():
                for nu in simulation_vals[n_comp].keys():
                    for nD in simulation_vals[n_comp][nu].keys():
                        for lag in simulation_vals[n_comp][nu][nD].keys():
                            for sliding_window_size in simulation_vals[n_comp][nu][nD][lag].keys():
                                for sim_type in simulation_vals[n_comp][nu][nD][lag][sliding_window_size].keys():
                                    if sim_type == 'fixed_freq':
                                        for freq_val in simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type].keys():
                                            naive_art_out_ratio = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type][freq_val].get('naive_ratio', 0)
                                            indepth_ratio = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type][freq_val].get('indepth_ratio', 0)
                                            repeated_counts = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type][freq_val].get('repeated_counts', 0)
                                            all_out_ratio = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type][freq_val].get('all_outliers_ratio', 0)
                                            time = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type][freq_val].get('time', 9999999)
                                            simulation_rows.append([sim_type, nu, nD, lag, sliding_window_size, freq_val, naive_art_out_ratio, indepth_ratio, repeated_counts, all_out_ratio, time, n_comp])
                                    else:
                                        naive_art_out_ratio = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type].get('naive_ratio', 0)
                                        indepth_ratio = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type].get('indepth_ratio', 0)
                                        repeated_counts = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type].get('repeated_counts', 0)
                                        all_out_ratio = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type].get('all_outliers_ratio', 0)
                                        time = simulation_vals[n_comp][nu][nD][lag][sliding_window_size][sim_type].get('time', 99999999)
                                        simulation_rows.append([sim_type, nu, nD, lag, sliding_window_size, None, naive_art_out_ratio, indepth_ratio, repeated_counts, all_out_ratio, time, n_comp])

        simulation_rows = pd.DataFrame(simulation_rows, columns=columns)
        dataset_report = pd.concat([dataset_report, simulation_rows], ignore_index=True)

        ### Step3 - remove uneligible configurations
        ### replace zero indepth naive ratio with min valie
        dataset_report['naive_artificial_outlier_detection_ratio'] = dataset_report['naive_artificial_outlier_detection_ratio'].mask(dataset_report['naive_artificial_outlier_detection_ratio']==0)
        dataset_report['naive_artificial_outlier_detection_ratio'] = dataset_report['naive_artificial_outlier_detection_ratio'].fillna(dataset_report['naive_artificial_outlier_detection_ratio'].min())
        dataset_report['indepth_artificial_outlier_detection_ratio'] = dataset_report['indepth_artificial_outlier_detection_ratio'].mask(dataset_report['indepth_artificial_outlier_detection_ratio']==0)
        dataset_report['indepth_artificial_outlier_detection_ratio'] = dataset_report['indepth_artificial_outlier_detection_ratio'].fillna(dataset_report['indepth_artificial_outlier_detection_ratio'].min())

        ###remove wrong fixed freq - sliding window configs
            # Banned Fixed freqs format [Sliding:Fixed_Freq]:
            # nyc_taxi_banned = {48: [336, 672], 336: [672], 672: [24]}
            # aapl_banned = {72: [144, 288, 864], 288: [12, 36, 864], 2016: [12, 36, 72]}
        banned_fixed_freqs_comb = {72: [144, 288, 864], 288: [12, 36, 864], 2016: [12, 36, 72]} if cur_dir == "realTweets" else {48: [336, 672], 336: [672], 672: [24]}
        for win_size, banned_fixed_freq_sizes in banned_fixed_freqs_comb.items():
            for banned_fixed_freq_size in banned_fixed_freq_sizes:
                dataset_report = dataset_report.drop(dataset_report[(dataset_report['sliding_window_size']==str(win_size)) & (dataset_report['fixed_frequency']==str(banned_fixed_freq_size))].index)


        ###Step4 - add a column which measures detection ratio compared to respective baseline for both naive and indepth detection
        ### (naive_ratio)/(all_ratio)/(naive_baseline_ratio/all_baseline_ratio) -> same for indepth
        dataset_report["naive_to_baseline_ratio"] = None
        dataset_report["indepth_to_baseline_ratio"] = None
        dataset_report_updated = pd.DataFrame()

        if method == "OCSVM":
            for nu in dataset_report.nu.unique():
                for dim in dataset_report.dimension.unique():
                    for lag in dataset_report.lag.unique():
                        records = dataset_report.copy().query("nu == \"{nu}\" and dimension == \"{dim}\" and lag == \"{lag}\"".format(
                            nu=nu,
                            dim=dim,
                            lag=lag,
                        ))
                        naive_baseline_ratio = (records[records['simulation_type'] == "Baseline"].naive_artificial_outlier_detection_ratio / records[records['simulation_type'] == "Baseline"].all_outliers_ratio).iloc[0]
                        indepth_baseline_ratio = (records[records['simulation_type'] == "Baseline"].indepth_artificial_outlier_detection_ratio / records[records['simulation_type'] == "Baseline"].all_outliers_ratio).iloc[0]

                        records.naive_to_baseline_ratio = records.naive_artificial_outlier_detection_ratio / records.all_outliers_ratio / naive_baseline_ratio
                        records.indepth_to_baseline_ratio = records.indepth_artificial_outlier_detection_ratio / records.all_outliers_ratio / indepth_baseline_ratio

                        dataset_report_updated = dataset_report_updated.append(records)
        elif method == "PCA":
            for nu in dataset_report.nu.unique():
                for n_comp in dataset_report.n_component.unique():
                    for dim in dataset_report.dimension.unique():
                        for lag in dataset_report.lag.unique():
                            records = dataset_report.copy().query("nu == \"{nu}\" and dimension == \"{dim}\" and lag == \"{lag}\" and n_component == \"{n_comp}\"".format(
                                    nu=nu,
                                    dim=dim,
                                    lag=lag,
                                    n_comp=n_comp,
                                ))
                            if records.empty:
                                continue
                            naive_baseline_ratio = (records[records['simulation_type'] == "Baseline"].naive_artificial_outlier_detection_ratio / records[records['simulation_type'] == "Baseline"].all_outliers_ratio).iloc[0]
                            indepth_baseline_ratio = (records[records[
                                                                  'simulation_type'] == "Baseline"].indepth_artificial_outlier_detection_ratio / records[records['simulation_type'] == "Baseline"].all_outliers_ratio).iloc[0]

                            records.naive_to_baseline_ratio = records.naive_artificial_outlier_detection_ratio / records.all_outliers_ratio / naive_baseline_ratio
                            records.indepth_to_baseline_ratio = records.indepth_artificial_outlier_detection_ratio / records.all_outliers_ratio / indepth_baseline_ratio

                            dataset_report_updated = dataset_report_updated.append(records)
        dataset_reports.append((dataset_name, dataset_report_updated))

    with pd.ExcelWriter("{}/{}_performance_overview.xlsx".format(path, cur_dir)) as writer:
        for name, df in dataset_reports:
            df.to_excel(writer, name)
        writer.save()


def visualise_simulation_with_baseline_comparison(root_path, cur_dir):
    path = "{}/analysis_OCSVM/ocsvm_simulation_comparison/{}".format(root_path, cur_dir)
    excel = pd.ExcelFile("{}/{}_performance_overview.xlsx".format(path, cur_dir))

    performances = {}
    for sheet_name in excel.sheet_names:
        performances[sheet_name] = excel.parse(sheet_name)

    for name, df in performances.items():
        print("Graph Visualisation is being prepared for {}".format(name))

        df.drop(['Unnamed: 0'], axis=1)
        df = df.sort_values(by=['naive_artificial_outlier_detection_ratio'])
        df = df.reset_index(drop=True)
        df = df.fillna('-1')
        df['Index'] = list(range(1, len(df)+1))

        for ratio_type in ["naive_artificial_outlier_detection_ratio", "indepth_artificial_outlier_detection_ratio"]:
            visualise_2d(root_path, cur_dir, df, name, ratio_type)
            visualise_3d(root_path, cur_dir, df, name, ratio_type)


def visualise_2d(root_path, cur_dir, df, name, ratio_type):
    colours, opacity, size, marker_type = get_visualisation_param(cur_dir)
    leg_vis, leg_desc = get_legend_for_vis(cur_dir)

    fig = plt.figure(figsize=(32, 16))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title("{} by different sliding window adaptations\nfor {}".format(ratio_type, name))
    ax.set_ylabel(ratio_type)
    ax.set_xlabel("Overall Outliers/All Data Points ratio")
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 0.8])
    ax.set_xticks(np.arange(0, 0.8, 0.05))
    ax.set_yticks(np.arange(0, 1, 0.1))

    for groupings1, rows1 in df.groupby(['simulation_type']):
        sim_type = groupings1
        for groupings2, rows2 in rows1.groupby(['dimension']):
            dim = groupings2
            for groupings3, rows3 in rows2.groupby(['sliding_window_size']):
                win_size = groupings3
                for groupings4, rows4 in rows3.groupby(['fixed_frequency']):
                    freq = groupings4
                    rows4.plot.scatter('all_outliers_ratio', ratio_type, ax=ax, color=colours.get(sim_type), alpha=opacity.get(dim), s=size.get(win_size, 15), marker=marker_type.get(freq, 'h'))

    ax.legend(leg_vis, leg_desc)

    dest_path = "{}/analysis_OCSVM/ocsvm_simulation_comparison/{}/performance_visualisation".format(root_path, cur_dir)
    if not os.path.isdir(dest_path):
        os.mkdir(dest_path)
    plt.savefig(os.path.join(dest_path, "{}_{}2d.png".format(ratio_type[:5], name)))
    plt.close()


def visualise_3d(root_path, cur_dir, df, name, ratio_type):
    colours, opacity, size, marker_type = get_visualisation_param(cur_dir)
    leg_vis, leg_desc = get_legend_for_vis(cur_dir)

    fig = plt.figure(figsize=(32, 16))
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.set_title("{} by different sliding window adaptations\nfor {}".format(ratio_type, name))
    ax.set_ylabel(ratio_type)
    ax.set_xlabel("Overall Outliers/All Datapoints ratio")
    ax.set_zlabel("Time [s]")
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 0.8])
    ax.set_zlim([0, 300])
    ax.set_xticks(np.arange(0, 0.8, 0.05))
    ax.set_yticks(np.arange(0, 1, 0.1))
    # ax.set_zticks(np.arange(0, 300, 1))

    for groupings1, rows1 in df.groupby(['simulation_type']):
        sim_type = groupings1
        for groupings2, rows2 in rows1.groupby(['dimension']):
            dim = groupings2
            for groupings3, rows3 in rows2.groupby(['sliding_window_size']):
                win_size = groupings3
                for groupings4, rows4 in rows3.groupby(['fixed_frequency']):
                    freq = groupings4
                    ax.scatter(rows4['all_outliers_ratio'], rows4[ratio_type], rows4['time'],
                               color=colours.get(sim_type), alpha=opacity.get(dim), s=size.get(win_size, 15),
                               marker=marker_type.get(freq, 'h'))

    ax.legend(leg_vis, leg_desc)

    dest_path = "{}/analysis_OCSVM/ocsvm_simulation_comparison/{}/performance_visualisation".format(root_path, cur_dir)
    if not os.path.isdir(dest_path):
        os.mkdir(dest_path)
    plt.savefig(os.path.join(dest_path, "{}_{}3d.png".format(ratio_type[:5], name)))
    plt.close()


def detect_unnecessary_param_config(root_path, cur_dir):
    path = "{}/analysis_OCSVM/ocsvm_simulation_comparison/{}".format(root_path, cur_dir)
    excel = pd.ExcelFile("{}/{}_performance_overview.xlsx".format(path, cur_dir))

    performances = {}
    for sheet_name in excel.sheet_names:
        performances[sheet_name] = excel.parse(sheet_name)

    wrong_configs = []
    for _, df in performances.items():
        df = df.sort_values(by=['all_outliers_ratio'])
        for i, row in df[df['all_outliers_ratio'] > 2*max(df['nu'])].iterrows():
            wrong_configs.append("sim_type={}_nu={}_dim={}_lag={}_winsize={}_fixfreq={}".format(
                row['simulation_type'], row['nu'], row['dimension'], row['lag'], row['sliding_window_size'], row['fixed_frequency']
            ))

    wrong_configs_final = []
    wrong_configs_counter = Counter(wrong_configs)
    for wrong_config in wrong_configs_counter:
        if wrong_configs_counter[wrong_config] > 0:
            wrong_configs_final.append(wrong_config)

    print(wrong_configs_final)


def create_report(root_dir, dir, lags, nDs):
    path = "{}/analysis_OCSVM".format(root_dir)
    if not os.path.isdir(path):
        os.makedirs(path)

    with open("{}/{}.txt".format(path, dir), "w") as f:
        df_collection = DfLoader().load_directory(os.path.join(root_dir, dir))
        for df_name in df_collection.get_dfs_names():
            print("working on {}/{}".format(dir, df_name))
            f.write("\nAnalysis of {}/{}\n\n".format(dir, df_name))
            df = df_collection.get_df(df_name)
            error_ratio_best = ("", 999)
            for lag in lags:
                for nD in nDs:
                    ocsvm = OneClassSVM(df, lag=lag, nD=nD)
                    ocsvm.model_fit_predict()
                    n_error_train = ocsvm.y_pred_train[ocsvm.y_pred_train == -1].size
                    n_error_test = ocsvm.y_pred_test[ocsvm.y_pred_test == -1].size
                    error_ratio_new = ("{}D_K={}".format(nD, lag), float(n_error_test) / len(ocsvm.y_pred_test))
                    if error_ratio_new[1] < error_ratio_best[1]:
                        error_ratio_best = error_ratio_new
                    line = "\n\tOne-Class SVM results for {}D, K={} are: Error train {}/{}, Error test {}/{}" \
                        .format(nD, lag, n_error_train, len(ocsvm.y_pred_train), n_error_test, len(ocsvm.y_pred_test))
                    f.write(line)
            f.write(
                "\nLowest ratio of errors for {}/{} was observed in {}: {}\n".format(dir, df_name, error_ratio_best[0],
                                                                                     error_ratio_best[1]))


def report_summary_best_clf(root_dir):
    path = "{}/analysis_OCSVM".format(root_dir)

    overview = {}
    for filename in os.listdir(path):
        with open(os.path.join(path, filename), 'r') as f:
            line = f.readline()
            while line:
                if "Lowest ratio of errors for" in line:
                    l = line.split("D_K=")
                    nd = int(l[0][-1])
                    lag = l[1][0]
                    if nd in overview.keys():
                        if lag in overview[nd].keys():
                            overview[nd][lag] += 1
                        else:
                            overview[nd][lag] = 1
                    else:
                        overview[nd] = {lag: 1}
                line = f.readline()

    with open("{}/summary.txt".format(path), "w") as s:
        for nd in overview.keys():
            for lag in overview[nd].keys():
                s.write("{}D, K={} was best classifier {} times\n".format(nd, lag, overview[nd][lag]))
            s.write("\n")


def report_summary_error_rates(root_dir):
    path = "{}/analysis_OCSVM".format(root_dir)

    scores = []
    for filename in os.listdir(path):
        with open(os.path.join(path, filename), 'r') as f:
            line = f.readline()
            while line:
                if "Lowest ratio of errors for" in line:
                    l = line.split(": ")
                    score = l[1][:-1]
                    scores += [float(score)]
                line = f.readline()

    scores.sort()
    with open("{}/summary_scores.txt".format(path), "w") as s:
        for score in scores:
            s.write("{}\n".format(score))


def find_best_kernel_params(root_dir, dir, df_name, df):
    lags = (1, 2, 3, 10, 20)
    nDs = (2, 3, 4)
    nu_vals = (0.01, 0.05, 0.1, 0.2, 0.5)
    gamma_vals = ('auto', 'scale')

    path = "{}/analysis_OCSVM/param_tuning/{}".format(root_dir, dir)
    if not os.path.isdir(path):
        os.makedirs(path)

    lowest_err_rate_overall = (999, "")

    with open("{}/{}.txt".format(path, df_name), "w") as f:
        for nD in nDs:
            lowest_err_rate_for_nd = (999, "")
            for lag in lags:
                lowest_err_rate_for_lag = (999, "")
                for nu in nu_vals:
                    for gamma in gamma_vals:
                        ocsvm = OneClassSVM(df, lag, nD)
                        ocsvm.model_fit_predict(nu, gamma)
                        n_error_test = ocsvm.y_pred_test[ocsvm.y_pred_test == -1].size
                        err_rate = float(n_error_test) / len(ocsvm.y_pred_test)
                        line = "\n{df_name}, {nD}D, K={lag}, nu={nu}, gamma={gamma}, Error Test:{n_error_test}/{len_test}, Error Rate={err_rate}".format(
                            df_name=df_name, nD=nD, lag=lag, nu=nu, gamma=gamma, n_error_test=n_error_test,
                            len_test=len(ocsvm.y_pred_test), err_rate=err_rate)
                        f.write(line)
                        if err_rate < lowest_err_rate_for_lag[0]:
                            lowest_err_rate_for_lag = (err_rate, line)
                        if err_rate < lowest_err_rate_for_nd[0]:
                            lowest_err_rate_for_nd = (err_rate, line)
                        if err_rate < lowest_err_rate_overall[0]:
                            lowest_err_rate_overall = (err_rate, line)
                f.write("\nLowest error for {}D, K={} was \n{}".format(nD, lag, lowest_err_rate_for_lag[1]))
            f.write("\nLowest error for {}D was \n{}".format(nD, lowest_err_rate_for_nd[1]))
        f.write("\nLowest error overall was for \n{}".format(lowest_err_rate_overall[1]))
