import os
from df_loader import DfLoader
from graph_creator_raw_data_visualisation import GraphCreatorRawDataVisualisation
from graph_creator_timeseries_visualisation import GraphCreatorTimeSeriesVisualisation
from graph_creator_one_class_svm import GraphCreatorOneClassSVM
from simulator_one_class_svm import SimulatorSVM
from analyse_excel import analyse_results, visualise_results, all_dataset_visualisation, performance_visualisation
import analysis_one_class_svm
import analysis_pca
import pandas as pd
import numpy as np


def plot_graphs(root_dir, dir, df_name, df, df_mod=None, outlier_indexes=None):
    gC = GraphCreatorRawDataVisualisation()
    gC.create_graph_with_artificial_outliers(dir, df_name, df, df_mod, outlier_indexes)
    gC.save_graph(root_dir, dir, df_name, outliers=True)
    gC.close_figure()
    print("{}_{}".format(dir, df_name))


def plot_ts(root_dir, dir, df_name, df):
    gC = GraphCreatorTimeSeriesVisualisation()
    gC.create_graph(dir, df_name, df)
    gC.save_graph(root_dir, dir, df_name)
    gC.close_figure()
    print("{}_{}".format(dir, df_name))


def plot_one_class_svm(root_dir, dir, df_name, df):
    print("working on {} file {}".format(dir, df_name))
    gC = GraphCreatorOneClassSVM()
    gC.create_graph(dir, df_name, df)
    gC.save_graph(root_dir, dir, df_name)
    gC.close_figure()
    print("OCSVM graphs created for {}_{}".format(root_dir, df_name))


def simulate_ocsvm_sliding_window(root_path, cur_dir, df_name, df):
    sim_ocsvm = SimulatorSVM(root_path, cur_dir, 1000, df_name, df)
    sim_ocsvm.simulate_dynamic(lag=1, nD=2)
    sim_ocsvm.simulate_continuous(lag=1, nD=2)
    sim_ocsvm.simulate_fixed_freq(fixed_freq=10, lag=1, nD=2)

def _calc_outlier(new_outlier, df, std_order, scale):
    if scale == "medium":
        multiplier = 2
    elif scale == "large":
        multiplier = 5
    else:
        multiplier = 1
    std_range = np.random.randint(low=50, high=500)
    df_slice = df[df.index.isin(list(range(new_outlier.name-std_range, new_outlier.name+std_range)))]
    slice_avg = np.mean(df_slice)
    slice_std = np.std(df_slice)
    new_outlier.values = np.random.uniform(low=slice_avg+(std_order-1)*slice_std*multiplier, high=slice_avg+std_order*slice_std*multiplier)[0]
    return new_outlier


def _generate_outliers(df, outliers_rate, scale="small"):
    num_outliers = int(len(df) * outliers_rate)
    outlier_indices = sorted(np.random.permutation(np.arange(len(df)))[:num_outliers])
    std1_indexes, std2_indexes, std3_indexes = outlier_indices[:int(0.68*num_outliers)], outlier_indices[int(0.68*num_outliers):int(0.95*num_outliers)], outlier_indices[int(0.95*num_outliers):]
    df.loc[df.index.isin(std1_indexes)] = df.loc[df.index.isin(std1_indexes)].apply(lambda x, df: _calc_outlier(x, df, 1, scale), args=(df,), axis=1)
    df.loc[df.index.isin(std2_indexes)] = df.loc[df.index.isin(std2_indexes)].apply(lambda x, df: _calc_outlier(x, df, 2, scale), args=(df,), axis=1)
    df.loc[df.index.isin(std3_indexes)] = df.loc[df.index.isin(std3_indexes)].apply(lambda x, df: _calc_outlier(x, df, 3, scale), args=(df,), axis=1)
    return df, outlier_indices

def load_df_with_outliers(root_dir, dir, df_name, outliers_rate=0.01, outliers_scale="small", rerun=False):
    mod_df_path = "{}/{}_modified".format(root_dir, dir)
    if not os.path.isdir(mod_df_path):
        os.makedirs(mod_df_path)
    if rerun:
        df = pd.read_csv("{}/{}_with_outliers.csv".format(mod_df_path, df_name, axis=1))
        with open("{}/{}_outliers_locs.txt".format(mod_df_path, df_name), "r") as f:
            outlier_indexes = [int(i) for i in f.read().split(", ")]
    else:
        df, outlier_indexes = _generate_outliers(df_collection.get_df(df_name), 0.01, scale="medium")
        df.to_csv("{}/{}_with_outliers.csv".format(mod_df_path, df_name))
        with open("{}/{}_outliers_locs.txt".format(mod_df_path, df_name), "w") as f:
            f.write(", ".join([str(i) for i in outlier_indexes]))
    return df, outlier_indexes

##main to run the experiment and visualise the results
if __name__ == '__main__':
    rerun = False #change in order not to create a new outliers but load ones from previous run
    root_dir = '/home/pawel/Uni/Dissertation/data/NAB'
    dirs = ["realTweets", "realKnownCause"]
    for dir in dirs:
        df_collection = DfLoader().load_directory(os.path.join(root_dir, dir))
        for df_name in df_collection.get_dfs_names():
            df, outlier_indexes = load_df_with_outliers(root_dir, dir, df_name, outliers_rate=0.01, outliers_scale="medium", rerun=False)

            nu_vals = [0.01, 0.05, 0.1]
            dims = [1, 2, 3, 4]
            lags = [1, 2, 5]
            n_components = [1, 2, 5, 10]
            if dir == "realKnownCause":
                sliding_windows = [48, 336, 672] #NYC TAXI one-day:48, one-week:336 and two week:672
                fixed_freqs = [24, 48, 336, 672] #NYC TAXI half-a-day:24, one-day:38 one-week:336, two-weeks:672
            elif dir == "realTweets":
                sliding_windows = [72, 288, 2016] #AAPL six-hours: 72 one-day:288, one-week:2016
                fixed_freqs = [12, 36, 72, 144, 288, 864] #AAPL one-hour:12 three-hours:36 six-hours:72 half-a-day:144, one-day:288 three-days:864

            analysis_one_class_svm.compare_simulation_with_baseline(df, root_dir, dir, df_name, outlier_indexes=outlier_indexes, nu_vals=nu_vals, dims=dims, lags=lags, sliding_window_sizes=sliding_windows, fixed_freqs=fixed_freqs)
            analysis_pca.compare_simulation_with_baseline(df, root_dir, dir, df_name, outlier_indexes=outlier_indexes, nu_vals=nu_vals, n_components=n_components, dims=dims, lags=lags, sliding_window_sizes=sliding_windows, fixed_freqs=fixed_freqs)
            #TESTanalysis_one_class_svm.compare_simulation_with_baseline(df, root_dir, dir, df_name, outlier_indexes=outlier_indexes, nu_vals=[0.1], dims=[2], lags=[1], sliding_window_sizes=[288], fixed_freqs=[144])
            #TESTanalysis_pca.compare_simulation_with_baseline(df, root_dir, dir, df_name, outlier_indexes=outlier_indexes, nu_vals=[0.1], n_components=[1], dims=[2], lags=[1], sliding_window_sizes=[288], fixed_freqs=[144])

        analysis_one_class_svm.report_simulation_with_baseline_comparison(root_dir, dir, method="PCA")
        analysis_one_class_svm.visualise_simulation_with_baseline_comparison(root_dir, dir)
        analyse_results(dir, method="PCA")
        visualise_results(dir)
    all_dataset_visualisation(root_dir, dirs[0], dirs[1], method="PCA")
    performance_visualisation(root_dir, dirs[0], dirs[1], method="OCSVM")
    print("End of execution")


# ### main to visualise simulation of ocsvm with sliding windows
# if __name__ == '__main__':
#     root_dir = '/home/pawel/Uni/Dissertation/data/NAB'
#     dirs = os.listdir(root_dir)
#     dirs = [d for d in dirs if os.path.isdir(os.path.join(root_dir, d)) and d not in ["graphs", "one_class_svm", "analysis_OCSVM"]]  # exclude dirs which are for output and don't have any TS within them
#     for dir in [dirs[0]]:
#         df_collection = DfLoader().load_directory(os.path.join(root_dir, dir))
#         for df_name in df_collection.get_dfs_names()[3:]:
#             df = df_collection.get_df(df_name)
#             simulate_ocsvm_sliding_window(root_dir, dir, df_name, df)


###main to plot data visualisation of ocsvm clf
# if __name__ == '__main__':
#     root_dir = '/home/pawel/Uni/Dissertation/data/NAB'
#     dirs = os.listdir(root_dir)
#     dirs = [d for d in os.listdir(root_dir) if os.path.isdir(os.path.join(root_dir, d)) and d in [
#         "realTweets"]]  # exclude dirs which are for output and don't have any TS within them
#     for dir in dirs:
#         df_collection = DfLoader().load_directory(os.path.join(root_dir, dir))
#         for df_name in df_collection.get_dfs_names():
#             df = df_collection.get_df(df_name)
#             df_mod, outlier_indexes = _generate_outliers(df.copy(), 0.01, scale="large")
#             outlier_indexes.sort()
#             plot_graphs(root_dir, dir, df_name, df, df_mod, outlier_indexes)


# ###main to analyze the results of one class svm
# if __name__ == '__main__':
#     root_dir = '/home/pawel/Uni/Dissertation/data/NAB'
#     dirs = os.listdir(root_dir)
#     dirs = [d for d in dirs if os.path.isdir(os.path.join(root_dir, d)) and d not in ["graphs", "one_class_svm", "analysis_OCSVM"]]  # exclude dirs which are for output and don't have any TS within them
#     # AnalysisOneCLassSVM().report_summary_best_clf(root_dir) #look up which param for classifier (lags, nD) was the best
#     # AnalysisOneCLassSVM().report_summary_error_rates(root_dir) #sum up error rates for OCSVM
#     for dir in dirs:
#         df_collection = DfLoader().load_directory(os.path.join(root_dir, dir))
#         # AnalysisOneCLassSVM().create_report(root_dir, dir, lags=(1, 2, 3, 10, 20), nDs=(2, 3, 4)) #create summary for each of the directories with output
#         for df_name in df_collection.get_dfs_names():
#             df = df_collection.get_df(df_name)
#             # AnalysisOneCLassSVM().find_best_kernel_params(root_dir, dir, df_name, df) #create report with summary of how different OCSVM param affect the outlier detection