import pandas as pd
import os
import logging

class DfCollection(object):
    def __init__(self, parent_path):
        self.parent_path = parent_path
        self.dfs = {}

    def add_df(self, df_name, df):
        df_name = df_name.split(".")[0]
        if df_name in self.dfs.keys():
            logging.info("Dataframe with a name: {} already exists or is empty".format(df_name))
            return
        self.dfs[df_name] = df

    def get_dfs_names(self):
        return list(self.dfs.keys())

    def get_df(self, df_name):
        return self.dfs.get(df_name)


class DfLoader(object):
    def load_df(self, file_path):
        if not os.path.isfile(file_path):
            logging.info("File {} cannot be found".format(file_path))
            return
        return pd.read_csv(file_path, names=["timestamp", "values"], header=0, index_col=False)

    def load_directory(self, dir_path):
        if not os.path.isdir(dir_path):
            logging.info("Directory {} cannot be found".format(dir_path))
            return
        df_collection = DfCollection(dir_path)
        for file in os.listdir(dir_path):
            df_collection.add_df(file, self.load_df(os.path.join(dir_path, file)))
        return df_collection