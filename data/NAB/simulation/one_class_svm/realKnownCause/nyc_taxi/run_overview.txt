

fixed_freq, 2D, K=1 realKnownCause/nyc_taxi
	Inliers detected 160
	Outliers detected 51
	Redundant retrains Retrains were nothing changed:0, check graph for insights
	Sliding window size 288
	Amount of datapoints 500
	True outliers detected (dynamic simulation only) N/A
	Fixed frequency size N/A

dynamic, 2D, K=1 realKnownCause/nyc_taxi
	Inliers detected 165
	Outliers detected 46
	Redundant retrains N/a
	Sliding window size 288
	Amount of datapoints 500
	True outliers detected (dynamic simulation only) 42
	Fixed frequency size N/A